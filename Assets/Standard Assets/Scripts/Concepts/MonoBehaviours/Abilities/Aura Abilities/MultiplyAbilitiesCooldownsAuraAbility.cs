using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace RTS
{
	public class MultiplyAbilitiesCooldownsAuraAbility : AuraAbility
	{
		public float multiplyAbilitiesCooldowns;
		Unit[] unitsToAffect = new Unit[0];

		public override void Do ()
		{
			if (!isCoolingDown && !doAnimation)
				ApplyMultiplyAbilitiesCooldownsAura ();
			base.Do ();
		}
		
		public void ApplyMultiplyAbilitiesCooldownsAura ()
		{
			unitsToAffect = AffectUnits((Unit unit) => { Apply (unit); });
			indicatorGo.SetActive(true);
		}

		public void UnapplyMultiplyAbilitiesCooldownsAura ()
		{
			for (int i = 0; i < unitsToAffect.Length; i ++)
			{
				Unit unit = unitsToAffect[i];
				Unapply (unit);
			}
			indicatorGo.SetActive(false);
		}

		void Apply (Unit unit)
		{
			for (int i = 0; i < unit.abilities.Length; i ++)
			{
				Ability ability = unit.abilities[i];
				ability.multiplyCooldownTimer *= multiplyAbilitiesCooldowns;
			}
		}

		void Unapply (Unit unit)
		{
			for (int i = 0; i < unit.abilities.Length; i ++)
			{
				Ability ability = unit.abilities[i];
				ability.multiplyCooldownTimer /= multiplyAbilitiesCooldowns;
			}
		}

		void OnDisable ()
		{
			if (channelTimeRemaining > 0)
				UnapplyMultiplyAbilitiesCooldownsAura ();
		}

		public override IEnumerator ChannelRoutine ()
		{
			cooldownIndicatorImage.fillAmount = 0;
			channelTimeRemaining = maxChannelTime;
			Unit[] previousUnitsToAffect = new Unit[0];
			while (channelTimeRemaining > 0 && shouldUse())
			{
				channelTimeRemaining -= Time.deltaTime;
				unitsToAffect = AffectUnits
				(
					(Unit unit) =>
					{
						if (!previousUnitsToAffect.Contains(unit))
							Apply (unit);
					}
				);
				for (int i = 0; i < previousUnitsToAffect.Length; i ++)
				{
					Unit unit = previousUnitsToAffect[i];
					if (!unitsToAffect.Contains(unit))
						Unapply (unit);
				}
				previousUnitsToAffect = new Unit[unitsToAffect.Length];
				unitsToAffect.CopyTo(previousUnitsToAffect, 0);
				yield return new WaitForEndOfFrame();
			}
			UnapplyMultiplyAbilitiesCooldownsAura ();
			channelTimeRemaining = 0;
			StartCoroutine(CooldownRoutine ());
		}
	}
}