using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace RTS
{
	public class AddToAnimatorFloatParameterAuraAbility : AuraAbility
	{
		public string parameterName;
		public float amount;
		List<Unit> previousUnitsToAffect = new List<Unit>();

		public override void Do ()
		{
			if (!isCoolingDown && !doAnimation)
				ApplyAddToAnimatorFloatParameterAura ();
			base.Do ();
		}
		
		public void ApplyAddToAnimatorFloatParameterAura ()
		{
			StartCoroutine(AuraRoutine ());
		}

		IEnumerator AuraRoutine ()
		{
			float startTime = Time.time;
			while (true)
			{
				AffectUnits ((Unit unit) =>
					{
						bool hasParameter = false;
						for (int i = 0; i < unit.animator.parameterCount; i ++)
						{
							if (unit.animator.GetParameter(i).name == parameterName)
							{
								hasParameter = true;
								break;
							}
						}
						if (hasParameter)
						{
							bool previouslyAffecting = previousUnitsToAffect.Contains(unit);
							bool isInRange = IsUnitInRange(unit);
							if (!previouslyAffecting && isInRange)
							{
								if (unit.type != source.type || (canAffectSelf && unit == source) || (canAffectSameUnitType && unit.type == source.type))
								{
									unit.animator.SetFloat(parameterName, unit.animator.GetFloat(parameterName) + amount);
									previousUnitsToAffect.Add(unit);
								}
							}
							else if (previouslyAffecting && !isInRange)
							{
								unit.animator.SetFloat(parameterName, unit.animator.GetFloat(parameterName) - amount);
								previousUnitsToAffect.Remove(unit);
							}
						}
					});
				yield return new WaitForEndOfFrame();
				if (Time.time - startTime > duration)
					break;
			}
			OnDisable ();
		}

		void OnDisable ()
		{
			for (int i = 0; i < previousUnitsToAffect.Count; i ++)
			{
				Unit unit = previousUnitsToAffect[i];
				unit.animator.SetFloat(parameterName, unit.animator.GetFloat(parameterName) - amount);
			}
			previousUnitsToAffect.Clear();
		}
	}
}