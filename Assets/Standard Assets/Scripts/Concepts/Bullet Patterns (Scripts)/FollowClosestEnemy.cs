﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace RTS
{
	[CreateAssetMenu]
	public class FollowClosestEnemy : AimWhereFacing
	{
		public int[] enemyPlayersIndices = new int[0];
		public float rotateRate;
		public float followRange;

		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab)
		{
			Bullet[] output = base.Shoot(spawner, bulletPrefab);
			for (int i = 0; i < output.Length; i ++)
			{
				Bullet bullet = output[i];
				bullet.StartCoroutine(FollowRoutine (bullet));
			}
			return output;
		}

		public virtual IEnumerator FollowRoutine (Bullet bullet)
		{
			while (true)
			{
				Vector2 closestEnemyPosition = new Vector2();
				float closestEnemyDistanceSqr = Mathf.Infinity;
				for (int i = 0; i < enemyPlayersIndices.Length; i ++)
				{
					int enemyPlayersIndex = enemyPlayersIndices[i];
					Player enemyPlayer = Game.players[enemyPlayersIndex];
					for (int i2 = 0; i2 < enemyPlayer.units.Length; i2 ++)
					{
						Unit enemyUnit = enemyPlayer.units[i2];
						if (!(enemyUnit is Building))
						{
							float enemyDistanceSqr = (enemyUnit.trs.position - bullet.trs.position).sqrMagnitude;
							if (enemyDistanceSqr < closestEnemyDistanceSqr && enemyDistanceSqr <= followRange * followRange)
							{
								closestEnemyPosition = enemyUnit.trs.position;
								closestEnemyDistanceSqr = enemyDistanceSqr;
							}
						}
					}
				}
				Vector3 velocity = Vector3.RotateTowards(bullet.rigid.velocity, closestEnemyPosition - (Vector2) bullet.trs.position, rotateRate * Mathf.Deg2Rad * Time.deltaTime, 0);
				bullet.rigid.velocity = velocity;
				bullet.trs.up = velocity;
				yield return new WaitForFixedUpdate();
			}
		}
	}
}