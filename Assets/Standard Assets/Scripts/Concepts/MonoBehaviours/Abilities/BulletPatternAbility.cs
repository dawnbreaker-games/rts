using UnityEngine;
using UnityEngine.Events;

namespace RTS
{
	public class BulletPatternAbility : Ability
	{
		public BulletPatternEntry bulletPatternEntry;
		public string bulletLayerName;
		public float damageMultiplier;
		public Unit shooter;

		public override void Do ()
		{
			if (!isCoolingDown && !doAnimation)
				Shoot ();
			base.Do ();
		}

		public virtual Bullet[] Shoot ()
		{
			Bullet[] output = bulletPatternEntry.Shoot();
			for (int i = 0; i < output.Length; i ++)
			{
				Bullet bullet = output[i];
				bullet.gameObject.layer = LayerMask.NameToLayer(bulletLayerName);
				bullet.damageMultiplier = damageMultiplier;
				bullet.shooter = shooter;
				bullet.Init ();
				bullet.collider.enabled = true;
			}
			return output;
		}
	}
}