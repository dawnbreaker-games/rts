using UnityEngine;

namespace RTS
{
	public class SpawnObjectAbility : Ability
	{
		public Transform spawnTrs;
		public float duration;
		public Spawnable spawnablePrefab;

		public override void Do ()
		{
			if (!isCoolingDown && !doAnimation)
				DoSpawnObject ();
			base.Do ();
		}

		public virtual void DoSpawnObject ()
		{
			Spawnable spawnable = ObjectPool.instance.SpawnComponent<Spawnable>(spawnablePrefab.prefabIndex, spawnTrs.position);
			if (duration > 0)
				ObjectPool.instance.DelayDespawn (spawnable.prefabIndex, spawnable.gameObject, spawnable.trs, duration);
		}
	}
}