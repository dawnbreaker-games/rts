using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace RTS
{
	public class ForceField : Spawnable
	{
		public float pushSpeed;

		void OnTriggerEnter2D (Collider2D collider)
		{
			Unit unit = collider.GetComponent<Unit>();
			if (unit != null)
			{
				Rigidbody2D rigid = unit.rigid;
				if (rigid != null)
					unit.extraVelocity = (Vector2) (unit.trs.position - trs.position).normalized * pushSpeed;
			}
			else
			{
				Bullet bullet = collider.GetComponent<Bullet>();
				Vector2 toBullet = bullet.trs.position - trs.position;
				bullet.trs.up = toBullet;
				bullet.rigid.velocity = toBullet.normalized * bullet.moveSpeed;
			}
		}

		void OnTriggerStay2D (Collider2D collider)
		{
			OnTriggerEnter2D (collider);
		}
	}
}