using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace RTS
{
	public class VirtualPlayer2 : Player
	{
		public AbilityCombinationInfo[] abilityCombinationInfos = new AbilityCombinationInfo[0];
		// public AbilityInfo[] abilityInfos = new AbilityInfo[0];
		// public float minDistanceToAverageEnemyPositionTillTargetClosestEnemyUnit;
		public float maxPositionOffset;
		public float maxDistanceToDogdeBullets;
		public byte enemyBuildingOffensiveWeight;
		public byte buildingDefensiveWeight;
		public float differenceInBuildingTotalHealthFractionToDefend;
		float useAbilityCombinationTime;
		bool[] usingAbilities = new bool[3];
		AbilityCombinationInfo abilityCombinationInfo;
		float buildingTotalHealthFraction = 1;
		float enemyBuildingTotalHealthFraction = 1;
		float buildingsTotalHealth;
		float enemyBuildingsTotalHealth;
		// Dictionary<byte, List<Unit>> unitTypesDict = new Dictionary<byte, List<Unit>>();
		// List<FloatRange> timeRangesToFaceEnemy = new List<FloatRange>();

		public override void OnEnable ()
		{
			base.OnEnable ();
			for (int i = 0; i < buildings.Length; i ++)
			{
				Building building = buildings[i];
				building.onTakeDamage += OnBuildingTakeDamage;
				buildingsTotalHealth += building.maxHp;
			}
			Player enemyPlayer = enemyPlayers[0];
			for (int i = 0; i < enemyPlayer.buildings.Length; i ++)
			{
				Building building = enemyPlayer.buildings[i];
				building.onTakeDamage += OnBuildingTakeDamage;
				enemyBuildingsTotalHealth += building.maxHp;
			}
		}

		public override void DoUpdate ()
		{
			HandleMovement ();
			HandleAbilities ();
			HealUnits ();
		}

		void HandleMovement ()
		{
			Vector2 averageUnitPosition = new Vector2();
			List<Vector2> unitPositions = new List<Vector2>();
			for (int i = 0; i < units.Length; i ++)
			{
				Unit unit = units[i];
				if (unit == null)
					return;
				unitPositions.Add(unit.trs.position);
			}
			if (unitPositions.Count > 0)
				averageUnitPosition = VectorExtensions.GetAverage(unitPositions.ToArray());
			MovementType currentMovementType = MovementType.PullUnits;
			List<Transform> enemyUnitTransforms = new List<Transform>();
			List<Vector2> enemyUnitPositions = new List<Vector2>();
			List<Vector2> enemyUnitAndMyBuildingPositions = new List<Vector2>();
			Vector2 averageEnemyPosition = new Vector2();
			Vector2 averageEnemyAndMyBuildingPosition = new Vector2();
			for (int i = 0; i < enemyPlayers.Length; i ++)
			{
				Player enemyPlayer = enemyPlayers[i];
				for (int i2 = 0; i2 < enemyPlayer.units.Length; i2 ++)
				{
					Unit enemyUnit = enemyPlayer.units[i2];
					if (!(enemyUnit is Building))
					{
						Transform enemyTrs = enemyUnit.trs;
						enemyUnitTransforms.Add(enemyTrs);
						enemyUnitPositions.Add(enemyTrs.position);
					}
				}
				for (int i2 = 0; i2 < buildings.Length; i2 ++)
				{
					Building building = buildings[i2];
					for (byte i3 = 0; i3 < buildingDefensiveWeight; i3 ++)
						enemyUnitAndMyBuildingPositions.Add(building.trs.position);
				}
				for (int i2 = 0; i2 < enemyPlayer.buildings.Length; i2 ++)
				{
					Building enemyBuilding = enemyPlayer.buildings[i2];
					Transform enemyTrs = enemyBuilding.trs;
					enemyUnitTransforms.Add(enemyTrs);
					for (byte i3 = 0; i3 < enemyBuildingOffensiveWeight; i3 ++)
						enemyUnitPositions.Add(enemyTrs.position);
				}
				if (enemyUnitPositions.Count > 0)
					averageEnemyPosition = VectorExtensions.GetAverage(enemyUnitPositions.ToArray());
				if (enemyUnitAndMyBuildingPositions.Count > 0)
					averageEnemyAndMyBuildingPosition = VectorExtensions.GetAverage(enemyUnitAndMyBuildingPositions.ToArray());
				List<Bullet> enemyBullets = new List<Bullet>(Bullet.playersBulletsDict[(byte) Game.players.IndexOf(enemyPlayer)]);
				if (unitPositions.Count > 0)
				{
					for (int i2 = 0; i2 < enemyBullets.Count; i2 ++)
					{
						Bullet enemyBullet = enemyBullets[i2];
						Vector2 toEnemyBullet = (Vector2) enemyBullet.trs.position - averageUnitPosition;
						if (toEnemyBullet.sqrMagnitude <= maxDistanceToDogdeBullets * maxDistanceToDogdeBullets)
						{
							cursor.MoveTo ((Vector2) cursor.trs.position - toEnemyBullet * cursor.targetMovementIndicatorLength);
							currentMovementType = MovementType.MoveUnits;
							break;
						}
					}
				}
			}
			if (currentMovementType == MovementType.PullUnits && enemyUnitPositions.Count > 0)
			{
				(Transform trs, float distanceSqr) closestEnemyTrsAndDistanceSqr;
				if (enemyBuildingTotalHealthFraction - buildingTotalHealthFraction < differenceInBuildingTotalHealthFractionToDefend)
					closestEnemyTrsAndDistanceSqr = TransformExtensions.GetClosestTransformAndDistanceSqr_2D(enemyUnitTransforms.ToArray(), averageEnemyPosition);
				else
					closestEnemyTrsAndDistanceSqr = TransformExtensions.GetClosestTransformAndDistanceSqr_2D(enemyUnitTransforms.ToArray(), averageEnemyAndMyBuildingPosition);
				// if (closestEnemyTrsAndDistanceSqr.distanceSqr < minDistanceToAverageEnemyPositionTillTargetClosestEnemyUnit * minDistanceToAverageEnemyPositionTillTargetClosestEnemyUnit)
				// 	cursor.MoveTo (averageEnemyPosition + Random.insideUnitCircle * maxPositionOffset);
				// else
					cursor.MoveTo ((Vector2) closestEnemyTrsAndDistanceSqr.trs.position + Random.insideUnitCircle * maxPositionOffset);
			}
			if (currentMovementType == MovementType.PullUnits)
			{
				pullMultiplier = 1;
				PullUnits ();
			}
			else if (currentMovementType == MovementType.PushUnits)
			{
				pullMultiplier = -1;
				PullUnits ();
			}
			else
				MoveUnits ();
		}

		public override void HandleAbilities ()
		{
			byte abilityCombinationIndex;
			useAbilityCombinationTime -= Time.deltaTime;
			if (useAbilityCombinationTime < 0)
			{
				List<AbilityCombinationInfo> _abilityCombinationInfos = new List<AbilityCombinationInfo>(abilityCombinationInfos);
				if (abilityCombinationInfo != null)
				{
					for (int i = 0; i < abilityCombinationInfo.abilityCombinationIndicesThatCantBeNext.Length; i ++)
					{
						int abilityCombinationIndexThatCantBeNext = abilityCombinationInfo.abilityCombinationIndicesThatCantBeNext[i];
						_abilityCombinationInfos.Remove(abilityCombinationInfos[abilityCombinationIndexThatCantBeNext]);
					}
				}
				// timeRangesToFaceEnemy.Clear();
				while (true)
				{
					int abilityCombinationInfoIndex = Random.Range(0, _abilityCombinationInfos.Count);
					abilityCombinationInfo = _abilityCombinationInfos[abilityCombinationInfoIndex];
					if (Random.value < abilityCombinationInfo.chance)
					// {
					// 	bool shouldUseAbilityCombination = true;
					// 	for (int i = 0; i < abilityInfos.Length; i ++)
					// 	{
					// 		AbilityInfo abilityInfo = abilityInfos[i];
					// 		if (abilityCombinationInfo.abilityIndices.Contains(abilityInfo.abilityIndex))
					// 		{
					// 			FloatRange timeRangeToFaceEnemyAfterUse = abilityInfo.timeRangeToFaceEnemyAfterUse;
					// 			if (timeRangeToFaceEnemyAfterUse.max > 0)
					// 				timeRangesToFaceEnemy.Add(new FloatRange(Time.time + timeRangeToFaceEnemyAfterUse.min, Time.time + timeRangeToFaceEnemyAfterUse.max));
					// 			List<Unit> unitsOfType = unitTypesDict[abilityInfo.unitType];
					// 			for (int i2 = 0; i2 < unitsOfType.Count; i2 ++)
					// 			{
					// 				Unit unit = unitsOfType[i2];
					// 				if (abilityInfo.maxRangeFromCursorToUse != Mathf.Infinity)
					// 				{
					// 					if ((unit.trs.position - cursor.trs.position).sqrMagnitude > abilityInfo.maxRangeFromCursorToUse * abilityInfo.maxRangeFromCursorToUse)
					// 					{
					// 						shouldUseAbilityCombination = false;
					// 						break;
					// 					}
					// 				}
					// 				else if (abilityInfo.maxRangeFromDifferentTypeAllyToUse != Mathf.Infinity)
					// 				{
					// 					List<byte> differentUnitTypes = new List<byte>(unitTypesDict.Keys);
					// 					differentUnitTypes.Remove(abilityInfo.unitType);
					// 					for (int i3 = 0; i3 < differentUnitTypes.Count; i3 ++)
					// 					{
					// 						List<Unit> unitsOfDifferentType = unitTypesDict[differentUnitTypes[i3]];
					// 						for (int i4 = 0; i4 < unitsOfDifferentType.Count; i4 ++)
					// 						{
					// 							Unit differentTypeUnit = unitsOfDifferentType[i3];
					// 							if ((unit.trs.position - differentTypeUnit.trs.position).sqrMagnitude > abilityInfo.maxRangeFromDifferentTypeAllyToUse * abilityInfo.maxRangeFromDifferentTypeAllyToUse)
					// 							{
					// 								shouldUseAbilityCombination = false;
					// 								break;
					// 							}
					// 						}
					// 						if (!shouldUseAbilityCombination)
					// 							break;
					// 					}
					// 				}
					// 				else
					// 				{
					// 					List<byte> unitTypes = new List<byte>(unitTypesDict.Keys);
					// 					for (int i3 = 0; i3 < unitTypes.Count; i3 ++)
					// 					{
					// 						unitsOfType = unitTypesDict[unitTypes[i3]];
					// 						for (int i4 = 0; i4 < unitsOfType.Count; i4 ++)
					// 						{
					// 							Unit allyUnit = unitsOfType[i3];
					// 							if ((unit.trs.position - allyUnit.trs.position).sqrMagnitude > abilityInfo.maxRangeFromAllyToUse * abilityInfo.maxRangeFromAllyToUse)
					// 							{
					// 								shouldUseAbilityCombination = false;
					// 								break;
					// 							}
					// 						}
					// 						if (!shouldUseAbilityCombination)
					// 							break;
					// 					}
					// 				}
					// 				if (!shouldUseAbilityCombination)
					// 					break;
					// 			}
					// 		}
					// 		if (!shouldUseAbilityCombination)
					// 			break;
					// 	}
					// 	if (shouldUseAbilityCombination)
					// 		break;
					// 	else
					// 	{
					// 		_abilityCombinationInfos.RemoveAt(abilityCombinationInfoIndex);
					// 		if (_abilityCombinationInfos.Count == 0)
					// 		{
					// 			abilityCombinationInfo = abilityCombinationInfos[Random.Range(0, abilityCombinationInfos.Length)];
								break;
					// 		}
					// 	}
					// }
				}
				useAbilityCombinationTime += abilityCombinationInfo.useTimeRange.Get(Random.value);
				for (int i = 0; i < 3; i ++)
					usingAbilities[i] = abilityCombinationInfo.abilityIndices.Contains((byte) i);
			}
			base.HandleAbilities ();
		}

		public override void OnGainedUnit (Unit unit)
		{
			unit.Init ();
			for (int i = 0; i < unit.abilities.Length; i ++)
			{
				Ability ability = unit.abilities[i];
				int i2 = i;
				ability.shouldUse = () => { return usingAbilities[i2]; };
			}
			// List<Unit> unitsOfType = new List<Unit>(new Unit[] { unit });
			// if (unitTypesDict.TryGetValue(unit.type, out unitsOfType))
			// {
			// 	unitsOfType.Add(unit);
			// 	unitTypesDict[unit.type] = unitsOfType;
			// }
			// else
			// 	unitTypesDict.Add(unit.type, unitsOfType);
			// unit.onDeath += (Unit unit) => { unitTypesDict[unit.type].Remove(unit); };
		}

		void OnBuildingTakeDamage (Unit unit, float amount)
		{
			if (unit.player == this)
				buildingTotalHealthFraction -= amount / buildingsTotalHealth;
			else
				enemyBuildingTotalHealthFraction -= amount / enemyBuildingsTotalHealth;
		}

		[Serializable]
		public class AbilityCombinationInfo
		{
			public byte[] abilityIndices = new byte[0];
			[Range(0, 1)]
			public float chance;
			public FloatRange useTimeRange = new FloatRange();
			public byte[] abilityCombinationIndicesThatCantBeNext = new byte[0];
		}

		// [Serializable]
		// public class AbilityInfo
		// {
		// 	public byte abilityIndex;
		// 	public float maxRangeFromCursorToUse;
		// 	public float maxRangeFromDifferentTypeAllyToUse;
		// 	public float maxRangeFromAllyToUse;
		// 	public byte unitType;
		// 	public FloatRange timeRangeToFaceEnemyAfterUse;
		// }
	}
}