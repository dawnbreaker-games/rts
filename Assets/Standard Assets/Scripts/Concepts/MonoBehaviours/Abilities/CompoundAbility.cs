using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace RTS
{
	public class CompoundAbility : Ability
	{
		public Ability[] abilities = new Ability[0];

		IEnumerator Start ()
		{
			List<Ability> _abilities = new List<Ability>(new Ability[] { this });
			yield return new WaitUntil(() => ( shouldUse != null ));
			while (_abilities.Count > 0)
			{
				Ability ability = _abilities[0];
				CompoundAbility compoundAbility = ability as CompoundAbility;
				if (compoundAbility != null)
					_abilities.AddRange(compoundAbility.abilities);
				if (ability.IsChannellable)
					ability.shouldUse = shouldUse;
				_abilities.RemoveAt(0);
			}
		}

		public override void Do ()
		{
			if (!isCoolingDown && !doAnimation)
				DoCompoundAbility ();
			base.Do ();
		}

		public void DoCompoundAbility ()
		{
			for (int i = 0; i < abilities.Length; i ++)
			{
				Ability ability = abilities[i];
				ability.Do ();
			}
		}
	}
}