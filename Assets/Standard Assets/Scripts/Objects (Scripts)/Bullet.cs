﻿using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;
using DelayedDespawn = RTS.ObjectPool.DelayedDespawn;

namespace RTS
{
	//[ExecuteInEditMode]
	public class Bullet : Spawnable
	{
		public Collider2D collider;
		public Rigidbody2D rigid;
		public float moveSpeed;
		public float lifetime;
		public SpriteRenderer spriteRenderer;
		public Color canRetargetColor;
		public Color cantRetargetColor;
		[HideInInspector]
		public uint timesRetargeted;
		[HideInInspector]
		public bool hasFinishedRetargeting;
		public float damageAmountOnHitDestructable;
		public float damageMultiplier;
		public uint maxHitsBeforeDespawn;
		public float spawnTime;
		public static List<Bullet> bullets = new List<Bullet>();
		[HideInInspector]
		public uint hitsBeforeDespawnRemaining;
		[HideInInspector]
		public float radius;
		[HideInInspector]
		public float radiusSqr;
		[HideInInspector]
		public DelayedDespawn delayedDespawn;
		[HideInInspector]
		public Unit shooter;
		public byte shooterPlayerIndex;
		public Action<Collider2D> onHit;
		public static Dictionary<byte, List<Bullet>> playersBulletsDict = new Dictionary<byte, List<Bullet>>();

		public virtual void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (trs == null)
					trs = GetComponent<Transform>();
				if (rigid == null)
					rigid = GetComponent<Rigidbody2D>();
				radius = collider.bounds.extents.x;
				radiusSqr = radius * radius;
				return;
			}
#endif
			rigid.velocity = trs.up * moveSpeed;
			if (lifetime > 0)
				delayedDespawn = ObjectPool.instance.DelayDespawn(prefabIndex, gameObject, trs, lifetime);
			hitsBeforeDespawnRemaining = maxHitsBeforeDespawn;
			spawnTime = Time.time;
			bullets.Add(this);
		}

		public void Init ()
		{
			List<Bullet> playerBullets = new List<Bullet>();
			shooterPlayerIndex = shooter.playerIndex;
			if (playersBulletsDict.TryGetValue(shooterPlayerIndex, out playerBullets))
			{
				playerBullets.Add(this);
				playersBulletsDict[shooterPlayerIndex] = playerBullets;
			}
			else
				playersBulletsDict.Add(shooterPlayerIndex, new List<Bullet>() { this });
		}

		public virtual void OnTriggerEnter2D (Collider2D other)
		{
			hitsBeforeDespawnRemaining --;
			if (onHit != null)
				onHit (other);
			IDestructable destructable = other.GetComponent<IDestructable>();
			if (destructable != null)
				destructable.TakeDamage (damageAmountOnHitDestructable * damageMultiplier);
			if (hitsBeforeDespawnRemaining == 0)
				Despawn ();
		}

		public virtual void Despawn ()
		{
			ObjectPool.instance.CancelDelayedDespawn (delayedDespawn);
			ObjectPool.instance.Despawn (prefabIndex, gameObject, trs);
			collider.enabled = false;
		}

		public virtual void Retarget (Vector2 direction, bool lastRetarget = false)
		{
			if (lastRetarget && hasFinishedRetargeting)
				return;
			trs.up = direction;
			rigid.velocity = trs.up * moveSpeed;
			timesRetargeted ++;
			if (lastRetarget)
			{
				hasFinishedRetargeting = true;
				spriteRenderer.color = cantRetargetColor;
				spriteRenderer.sortingOrder --;
			}
		}

		public virtual void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			timesRetargeted = 0;
			if (hasFinishedRetargeting)
			{
				hasFinishedRetargeting = false;
				spriteRenderer.color = canRetargetColor;
				spriteRenderer.sortingOrder ++;
			}
			StopAllCoroutines();
			playersBulletsDict[shooterPlayerIndex].Remove(this);
			bullets.Remove(this);
		}
	}
}
