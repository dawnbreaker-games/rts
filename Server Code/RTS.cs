using System;
using System.Linq;
using PlayerIO.GameLibrary;

[RoomType("Default")]
public class RTS : Game<Player>
{
	long ticksAtStartOfGame;
	uint firstPlayerRaceIndex;

	public override void GameStarted ()
	{
		ticksAtStartOfGame = DateTime.UtcNow.Ticks;
	}

	public override void UserJoined (Player player)
	{
		if (PlayerCount <= 1)
			firstPlayerRaceIndex = uint.Parse(RoomData["lastPlayerRaceIndex"]);
		foreach (Player player2 in Players)
		{
			if (player2.Id != player.Id)
			{
				player.Send(MessageTypes.GAME_STARTED, ticksAtStartOfGame, firstPlayerRaceIndex);
				player2.Send(MessageTypes.GAME_STARTED, ticksAtStartOfGame, uint.Parse(RoomData["lastPlayerRaceIndex"]));
			}
		}
	}

	public override void UserLeft (Player player)
	{
		foreach (Player player2 in Players)
		{
			if (player2.Id != player.Id)
				player2.Send(MessageTypes.PLAYER_LEFT);
		}
	}

	public override void GotMessage (Player player, Message message)
	{
		foreach (Player player2 in Players)
		{
			if (player2.Id != player.Id)
				player2.Send(message);
		}
	}

	public struct MessageTypes
	{
		public const string PLAYER_MOVED_CURSOR = "Player moved cursor";
		public const string PLAYER_SET_MOVEMENT_MODE = "Player set movement mode";
		public const string PLAYER_TOGGLED_ABILITY_KEY = "Player toggled ability key";
		public const string PLAYER_LEFT = "Player left";
		public const string GAME_STARTED = "Game started";
		public const string UPDATE_UNIT_INFO = "Update unit info";
		public const string UNIT_DIED = "Unit died";
	}
}