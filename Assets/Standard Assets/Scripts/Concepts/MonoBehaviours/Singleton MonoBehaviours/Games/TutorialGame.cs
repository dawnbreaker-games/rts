using UnityEngine;

namespace RTS
{
	public class TutorialGame : Game
	{
		public new static TutorialGame instance;
		public new static TutorialGame Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<TutorialGame>();
				return instance;
			}
		}
		public Transform playersParent;

		// Used only by UI
		public void Retry ()
		{
			playersParent.gameObject.SetActive(false);
			playersParent = Instantiate(playersParent);
			Destroy(players[0].gameObject);
			Player enemyPlayer = players[1];
			Destroy(enemyPlayer.gameObject);
			for (int i = 0; i < enemyPlayer.units.Length; i ++)
			{
				Unit enemyUnit = enemyPlayer.units[i];
				Destroy(enemyUnit.gameObject);
			}
			for (int i = 0; i < enemyPlayer.buildings.Length; i ++)
			{
				Building enemyBuilding = enemyPlayer.buildings[i];
				Destroy(enemyBuilding.gameObject);
			}
			for (int i = 0; i < Bullet.bullets.Count; i ++)
			{
				Bullet bullet = Bullet.bullets[i];
				bullet.Despawn ();
				i --;
			}
			playersParent.gameObject.SetActive(true);
			Awake ();
		}
	}
}