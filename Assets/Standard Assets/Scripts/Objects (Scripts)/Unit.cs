using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using System.Collections.Generic;

namespace RTS
{
	public class Unit : Spawnable, IDestructable
	{
		public float hp;
		public float Hp
		{
			get
			{
				return hp;
			}
			set
			{
				hp = value;
			}
		}
		public float maxHp;
		public float MaxHp
		{
			get
			{
				return maxHp;
			}
			set
			{
				maxHp = value;
			}
		}
		public Rigidbody2D rigid;
		public Collider2D collider;
		public float moveSpeed;
		public float MoveSpeed
		{
			get
			{
				return moveSpeed;
			}
			set
			{
				moveSpeed = value;
			}
		}
		public byte playerIndex;
		// [HideInInspector]
		public Player player;
		public Image hpIndicatorImage;
		public Ability[] abilities = new Ability[0];
		public Transform uiTrs;
#if UNITY_EDITOR
		public Transform abilityIndicatorsParent;
		public Transform hpIndicatorTrs;
#endif
		[HideInInspector]
		public float radius;
		[HideInInspector]
		public float radiusSqr;
		public byte type;
		public float hpRegenRate;
		public Animator animator;
		public Action<Unit, float> onHeal;
		public Action<Unit, float> onTakeDamage;
		public Action<Unit> onDeath;
		// [HideInInspector]
		// public float spawnTime;
		[HideInInspector]
		public Vector2 extraVelocity;
		public SpriteRenderer spriteRenderer;
		[HideInInspector]
		public SerializableDictionary<string, BulletPatternAbility> bulletPatternAbilitiesDict = new SerializableDictionary<string, BulletPatternAbility>();
		protected bool dead;

		public virtual void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (trs == null)
					trs = GetComponent<Transform>();
				if (uiTrs == null)
				{
					Canvas canvas = GetComponentInChildren<Canvas>();
					if (canvas != null)
						uiTrs = canvas.GetComponent<Transform>();
				}
				uiTrs.SetWorldScale (Vector3.one);
				if (hpIndicatorTrs == null)
					hpIndicatorTrs = uiTrs.Find("Hp Indicator");
				if (hpIndicatorImage == null)
					hpIndicatorImage = hpIndicatorTrs.GetComponent<Image>();
				hpIndicatorTrs.SetWorldScale (trs.lossyScale);
				if (abilityIndicatorsParent == null)
					abilityIndicatorsParent = uiTrs.Find("Horizontal Layout Group");
				abilityIndicatorsParent.localPosition = Vector2.up * trs.lossyScale.y / 2;
				if (rigid == null)
					rigid = GetComponent<Rigidbody2D>();
				if (collider == null)
					collider = GetComponent<Collider2D>();
				if (collider != null)
				{
					radius = collider.bounds.extents.x;
					radiusSqr = radius * radius;
				}
				// Game.Instance.Awake ();
				if (Game.players.Length > playerIndex)
					player = Game.players[playerIndex];
				if (animator == null)
					animator = GetComponent<Animator>();
				bulletPatternAbilitiesDict.Clear();
				Ability[] abilities = GetComponents<Ability>();
				for (int i = 0; i < abilities.Length; i ++)
				{
					Ability ability = abilities[i];
					AuraAbility auraAbility = ability as AuraAbility;
					if (auraAbility != null && auraAbility.source == null)
						auraAbility.source = this;
					BulletPatternAbility bulletPatternAbility = ability as BulletPatternAbility;
					if (bulletPatternAbility != null)
						bulletPatternAbilitiesDict.Add(bulletPatternAbility.bulletPatternEntry.name, bulletPatternAbility);
					if (ability.animationEntry.animator == null)
						ability.animationEntry.animator = animator;
				}
				if (spriteRenderer == null)
					spriteRenderer = GetComponent<SpriteRenderer>();
				return;
			}
#endif
			bulletPatternAbilitiesDict.Init ();
			dead = false;
			// hp = maxHp;
			hpIndicatorImage.fillAmount = 1;
			// spawnTime = Time.time;
			extraVelocity = Vector2.zero;
		}

		public void Init ()
		{
			AIAgent aiAgent = AIAgent.instance;
			onDeath = null;
			if (aiAgent != null)
			{
				onHeal = aiAgent.AddHealReward;
				onTakeDamage = aiAgent.AddDamageReward;
				onDeath += aiAgent.AddDeathReward;
			}
			if (MultiplayerGame.instance != null && player == Game.localPlayer)
				onDeath += NetworkManager.OnLocalUnitDied;
		}

		public void Move (Vector2 targetMove)
		{
			Vector2 move = Vector2.ClampMagnitude(targetMove, moveSpeed);
			rigid.velocity = move + extraVelocity;
			extraVelocity *= 1f - Time.deltaTime * rigid.drag;
		}

		public virtual void SetFacing (Vector2 facing)
		{
			trs.up = facing;
			uiTrs.up = Vector2.up;
		}

		public void Heal (float amount)
		{
			float previousHp = hp;
			hp += amount;
			if (hp > maxHp)
				hp = maxHp;
			hpIndicatorImage.fillAmount = hp / maxHp;
			if (onHeal != null)
				onHeal (this, hp - previousHp);
		}

		public void TakeDamage (float amount)
		{
			float previousHp = hp;
			hp -= amount;
			hpIndicatorImage.fillAmount = hp / maxHp;
			if (hp <= 0)
			{
				hp = 0;
				if (MultiplayerGame.instance == null || player == Game.localPlayer)
					Death ();
				dead = true;
			}
			if (onTakeDamage != null)
				onTakeDamage (this, previousHp - hp);
		}

		public virtual void Death ()
		{
			if (dead)
				return;
			ObjectPool.instance.Despawn (prefabIndex, gameObject, trs);
			OnDeath ();
		}

		protected void OnDeath ()
		{
			player.units = player.units.Remove(this);
			if (player == null)
			{
				TutorialGame.Instance.GameOver ();
				return;
			}	
			if (onDeath != null)
			{
				onDeath (this);
				onDeath = null;
			}
			onHeal = null;
			onTakeDamage = null;
			if (player.units.Length == 0 && player.buildings.Length == 0)
				Game.instance.GameOver ();
		}

		public void DoAbility (int index)
		{
			abilities[index].Do ();
		}

		public void ShootBulletPatternAbility (string name)
		{
			bulletPatternAbilitiesDict[name].Shoot ();
		}

		public virtual void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			hp = maxHp;
		}
	}
}