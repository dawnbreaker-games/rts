using Extensions;
using UnityEngine;

namespace RTS
{
	public class DuplicateSelfAbility : Ability
	{
		public Unit unit;

		public override void Do ()
		{
			if (!isCoolingDown && !doAnimation)
				DuplicateSelf ();
			base.Do ();
		}

		public void DuplicateSelf ()
		{
			Unit newUnit = Instantiate(unit, unit.trs.position + unit.trs.up * unit.radius * 2, unit.trs.rotation);
			Ability newUnitAbility = newUnit.abilities[unit.abilities.IndexOf<Ability>(this)];
			newUnitAbility.isCoolingDown = true;
			newUnitAbility.StartCoroutine(newUnitAbility.CooldownRoutine ());
			unit.player.GainUnit (newUnit);
		}
	}
}