using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace RTS
{
	public class DeleteBulletsAbility : Ability
	{
		public Unit source;
		public float range;
		public byte[] playerIndicesOfBulletsToDelete = new byte[0];
		public GameObject indicatorGo;

#if UNITY_EDITOR
		void OnValidate ()
		{
			Transform indicatorTrs = indicatorGo.GetComponent<Transform>();
			indicatorTrs.SetWorldScale (Vector3.one * (range + source.radius) * 2);
		}
#endif

		public override void Do ()
		{
			if (!isCoolingDown && !doAnimation)
				DoDeleteBullets ();
			base.Do ();
		}

		public void DoDeleteBullets ()
		{
			for (int i = 0; i < playerIndicesOfBulletsToDelete.Length; i ++)
			{
				byte playerIndexOfBulletsToDelete = playerIndicesOfBulletsToDelete[i];
				List<Bullet> playerBullets = Bullet.playersBulletsDict[playerIndexOfBulletsToDelete];
				for (int i2 = 0; i2 < playerBullets.Count; i2 ++)
				{
					Bullet bullet = playerBullets[i2];
					if ((bullet.trs.position - source.trs.position).magnitude <= range + source.radius + bullet.radius)
						bullet.Despawn ();
				}
			}
		}
	}
}