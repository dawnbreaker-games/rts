using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace RTS
{
	public class AddToDamageMultiplierAuraAbility : AuraAbility
	{
		public float addToDamageMultiplier;

		public override void Do ()
		{
			if (!isCoolingDown && !doAnimation)
				ApplyAddToDamageMultiplierAura ();
			base.Do ();
		}
		
		public void ApplyAddToDamageMultiplierAura ()
		{
			List<BulletPatternAbility> abilitiesToAffect = new List<BulletPatternAbility>();
			AffectUnits ((Unit unit) => 
				{
					for (int i3 = 0; i3 < unit.abilities.Length; i3 ++)
					{
						Ability ability = unit.abilities[i3];
						BulletPatternAbility bulletPatternAbility = ability as BulletPatternAbility;
						if (bulletPatternAbility != null)
						{
							bulletPatternAbility.damageMultiplier += addToDamageMultiplier;
							abilitiesToAffect.Add(bulletPatternAbility);
						}
					}
				});
			if (abilitiesToAffect.Count > 0)
				StartCoroutine(AuraRoutine (abilitiesToAffect.ToArray()));
		}

		IEnumerator AuraRoutine (BulletPatternAbility[] abilitiesToAffect)
		{
			yield return new WaitForSeconds(duration);
			for (int i = 0; i < abilitiesToAffect.Length; i ++)
			{
				BulletPatternAbility ability = abilitiesToAffect[i];
				ability.damageMultiplier -= addToDamageMultiplier;
			}
		}
	}
}