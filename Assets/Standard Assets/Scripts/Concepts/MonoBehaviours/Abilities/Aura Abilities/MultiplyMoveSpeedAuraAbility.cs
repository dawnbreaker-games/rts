using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace RTS
{
	public class MultiplyMoveSpeedAuraAbility : AuraAbility
	{
		public float multiplyMoveSpeed;
		Unit[] unitsToAffect = new Unit[0];

		public override void Do ()
		{
			if (!isCoolingDown && !doAnimation)
				ApplyMultiplyMoveSpeedAura ();
			base.Do ();
		}
		
		public void ApplyMultiplyMoveSpeedAura ()
		{
			unitsToAffect = AffectUnits((Unit unit) => { Apply (unit); });
			indicatorGo.SetActive(true);
		}

		public void UnapplyMultiplyMoveSpeedAura ()
		{
			for (int i = 0; i < unitsToAffect.Length; i ++)
			{
				Unit unit = unitsToAffect[i];
				Unapply (unit);
			}
			indicatorGo.SetActive(false);
		}

		void Apply (Unit unit)
		{
			unit.moveSpeed *= multiplyMoveSpeed;
			for (int i = 0; i < unit.abilities.Length; i ++)
			{
				Ability ability = unit.abilities[i];
				SetSelfMoveSpeedAbility setSelfMoveSpeedAbility = ability as SetSelfMoveSpeedAbility;
				if (setSelfMoveSpeedAbility != null)
					setSelfMoveSpeedAbility.moveSpeed *= multiplyMoveSpeed;
			}
		}

		void Unapply (Unit unit)
		{
			unit.moveSpeed /= multiplyMoveSpeed;
			for (int i = 0; i < unit.abilities.Length; i ++)
			{
				Ability ability = unit.abilities[i];
				SetSelfMoveSpeedAbility setSelfMoveSpeedAbility = ability as SetSelfMoveSpeedAbility;
				if (setSelfMoveSpeedAbility != null)
					setSelfMoveSpeedAbility.moveSpeed /= multiplyMoveSpeed;
			}
		}

		void OnDisable ()
		{
			if (channelTimeRemaining > 0)
				UnapplyMultiplyMoveSpeedAura ();
		}

		public override IEnumerator ChannelRoutine ()
		{
			cooldownIndicatorImage.fillAmount = 0;
			channelTimeRemaining = maxChannelTime;
			Unit[] previousUnitsToAffect = new Unit[0];
			while (channelTimeRemaining > 0 && shouldUse())
			{
				channelTimeRemaining -= Time.deltaTime;
				unitsToAffect = AffectUnits
				(
					(Unit unit) =>
					{
						if (!previousUnitsToAffect.Contains(unit))
							Apply (unit);
					}
				);
				for (int i = 0; i < previousUnitsToAffect.Length; i ++)
				{
					Unit unit = previousUnitsToAffect[i];
					if (!unitsToAffect.Contains(unit))
						Unapply (unit);
				}
				previousUnitsToAffect = new Unit[unitsToAffect.Length];
				unitsToAffect.CopyTo(previousUnitsToAffect, 0);
				yield return new WaitForEndOfFrame();
			}
			UnapplyMultiplyMoveSpeedAura ();
			channelTimeRemaining = 0;
			StartCoroutine(CooldownRoutine ());
		}
	}
}