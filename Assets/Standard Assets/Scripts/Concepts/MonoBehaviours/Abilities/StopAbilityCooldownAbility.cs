using UnityEngine;

namespace RTS
{
	public class StopAbilityCooldownAbility : Ability
	{
		public Ability ability;

		public override void Do ()
		{
			if (!isCoolingDown && !doAnimation)
				StopAbilityCooldown ();
			base.Do ();
		}

		public void StopAbilityCooldown ()
		{
			ability.StopAllCoroutines();
			ability.isCoolingDown = false;
			ability.cooldownTimeRemaining = 0;
			ability.cooldownIndicatorImage.fillAmount = 1;
			ability.cooldownIndicatorImage.color = rechargedColor;
		}
	}
}