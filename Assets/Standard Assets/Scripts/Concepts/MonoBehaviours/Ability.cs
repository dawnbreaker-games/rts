using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.InputSystem.Controls;

namespace RTS
{
	public class Ability : MonoBehaviour
	{
		public Func<bool> shouldUse;
		public AnimationEntry animationEntry;
		public float cooldown;
		[HideInInspector]
		public bool isCoolingDown;
		public Image cooldownIndicatorImage;
		public Color rechargingColor;
		public Color rechargedColor;
		public float maxChannelTime;
		public bool doAnimation;
		[HideInInspector]
		public float cooldownTimeRemaining;
		[HideInInspector]
		public float channelTimeRemaining;
		[HideInInspector]
		public float multiplyCooldownTimer = 1;
		public float animationNormalizedTime = float.NegativeInfinity;
		public bool IsChannellable
		{
			get
			{
				return maxChannelTime > 0;
			}
		}

		public virtual void OnEnable ()
		{
			isCoolingDown = false;
		}

		public virtual void Do ()
		{
			if (isCoolingDown)
				return;
			isCoolingDown = true;
			if (!IsChannellable)
				StartCoroutine(CooldownRoutine ());
			else
				StartCoroutine(ChannelRoutine ());
			if (doAnimation)
				animationEntry.Play (animationNormalizedTime);
		}

		public virtual IEnumerator ChannelRoutine ()
		{
			cooldownIndicatorImage.fillAmount = 0;
			channelTimeRemaining = maxChannelTime;
			while (channelTimeRemaining > 0 && shouldUse())
			{
				channelTimeRemaining -= Time.deltaTime;
				yield return new WaitForEndOfFrame();
			}
			channelTimeRemaining = 0;
			StartCoroutine(CooldownRoutine ());
		}

		public IEnumerator CooldownRoutine ()
		{
			cooldownIndicatorImage.color = rechargingColor;
			cooldownTimeRemaining = cooldown;
			while (cooldownTimeRemaining > 0)
			{
				cooldownIndicatorImage.fillAmount = 1f - cooldownTimeRemaining / cooldown;
				cooldownTimeRemaining -= Time.deltaTime * multiplyCooldownTimer;
				yield return new WaitForEndOfFrame();
			}
			cooldownIndicatorImage.fillAmount = 1;
			cooldownIndicatorImage.color = rechargedColor;
			isCoolingDown = false;
		}
	}
}