using System;
using Extensions;
using UnityEngine;
using PlayerIOClient;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace RTS
{
	public class NetworkManager : SingletonMonoBehaviour<NetworkManager>
	{
		public Transform trs;
		public float updateUnitsInterval;
		static bool isFirstPlayer;
		static Connection connection;
		static List<Action> actions = new List<Action>();
		static Client client;
		static long ticksAtStartOfGame;
		static LocalPlayerUpdater localPlayerUpdater = new LocalPlayerUpdater();
		static EnemyPlayerUpdater enemyPlayerUpdater = new EnemyPlayerUpdater();

		public override void Awake ()
		{
			trs.SetParent(null);
			ticksAtStartOfGame = 0;
			base.Awake ();
		}

		public void Connect ()
		{
			PlayerIO.UseSecureApiRequests = true;
			PlayerIO.Authenticate("rts-fniyvumdiecjy9xn172tmg",
				"public",
				new Dictionary<string, string> {
					{ "userId", "default" },
				},
				null,
				OnConnectSucess,
				OnConnectFail
			);
		}

		void OnConnectSucess (Client client)
		{
			print("OnConnectSuccess");
			NetworkManager.client = client;
			client.Multiplayer.ListRooms("Default", new Dictionary<string, string>() { { "open", "true" } }, 0, 0, OnListOpenRoomsSuccess, OnListOpenRoomsFail);
		}

		void OnConnectFail (PlayerIOError error)
		{
			print("OnConnectFail: " + error);
			MainMenu.instance.canvasGroup.interactable = true;
		}

		void OnListOpenRoomsSuccess (RoomInfo[] roomInfos)
		{
			print("OnListOpenRoomsSuccess");
			if (roomInfos.Length == 0)
				client.Multiplayer.ListRooms("0", new Dictionary<string, string>() { { "open", "false" } }, 0, 0, OnListClosedRoomsSuccess, OnListClosedRoomsFail);
			else
			{
				RoomInfo roomInfo = roomInfos[0];
				Game.enemyPlayerRaceIndex = byte.Parse(roomInfo.RoomData["lastPlayerRaceIndex"]);
				isFirstPlayer = false;
				client.Multiplayer.CreateJoinRoom(roomInfo.Id, "Default", true, new Dictionary<string, string>() { { "open", "true" }, { "lastPlayerRaceIndex", "" + Game.localPlayerRaceIndex } }, new Dictionary<string, string>() { { "open", "false" } }, OnCreateJoinRoomSuccess, OnCreateJoinRoomFail);
				print("Joining room " + roomInfo.Id);
			}
		}

		void OnListOpenRoomsFail (PlayerIOError error)
		{
			print("OnListOpenRoomsFail: " + error);
			MainMenu.instance.canvasGroup.interactable = true;
		}

		void OnListClosedRoomsSuccess (RoomInfo[] roomInfos)
		{
			print("OnListClosedRoomsSuccess");
			isFirstPlayer = true;
			client.Multiplayer.CreateJoinRoom("" + roomInfos.Length, "Default", true, new Dictionary<string, string>() { { "open", "true" }, { "lastPlayerRaceIndex", "" + Game.localPlayerRaceIndex } }, new Dictionary<string, string>() { { "open", "false" } }, OnCreateJoinRoomSuccess, OnCreateJoinRoomFail);
			print("Creating room " + roomInfos.Length);
		}

		void OnListClosedRoomsFail (PlayerIOError error)
		{
			print("OnListClosedRoomsFail: " + error);
			MainMenu.instance.canvasGroup.interactable = true;
		}

		void OnCreateJoinRoomSuccess (Connection connection)
		{
			print("OnCreateJoinRoomSucess");
			NetworkManager.connection = connection;
			SceneManager.sceneLoaded += OnSceneLoaded;
			_SceneManager.instance.LoadSceneWithoutTransition ("Multiplayer");
		}

		void OnCreateJoinRoomFail (PlayerIOError error)
		{
			print("OnCreateJoinRoomFail: " + error);
			MainMenu.instance.canvasGroup.interactable = true;
		}

		void OnSceneLoaded (Scene scene = new Scene(), LoadSceneMode loadSceneMode = LoadSceneMode.Single)
		{
			connection.AddOnMessage(OnMessage);
		}

		void OnMessage (object sender, Message message)
		{
			print(message.Type);
			if (message.Type == MessageTypes.PLAYER_MOVED_CURSOR)
				actions.Add(new Action(new Vector2(message.GetFloat(0), message.GetFloat(1)), message.GetUInt(2)));
			else if (message.Type == MessageTypes.PLAYER_SET_MOVEMENT_TYPE)
				actions.Add(new Action((byte) message.GetUInt(0), message.GetUInt(1)));
			else if (message.Type == MessageTypes.PLAYER_TOGGLED_ABILITY_KEY)
				actions.Add(new Action(new bool[] { message.GetBoolean(0), message.GetBoolean(1), message.GetBoolean(2) }, message.GetUInt(3)));
			else if (message.Type == MessageTypes.GAME_STARTED)
			{
				MultiplayerGame.instance.waitForOtherPlayerIndicatorGo.SetActive(false);
				ticksAtStartOfGame = message.GetLong(0);
				Game.enemyPlayerRaceIndex = (byte) message.GetUInt(1);
				if (!isFirstPlayer)
				{
					Player[] potentialLocalPlayers = new Player[MultiplayerGame.instance.potentialLocalPlayers.Length];
					MultiplayerGame.instance.potentialLocalPlayers.CopyTo(potentialLocalPlayers, 0);
					MultiplayerGame.instance.potentialEnemyPlayers.CopyTo(MultiplayerGame.instance.potentialLocalPlayers, 0);
					potentialLocalPlayers.CopyTo(MultiplayerGame.instance.potentialEnemyPlayers, 0);
				}
				MultiplayerGame.instance.Init ();
				GameManager.updatables = GameManager.updatables.Add(localPlayerUpdater);
				GameManager.updatables = GameManager.updatables.Add(enemyPlayerUpdater);
				Game.localPlayer.enabled = true;
				Game.localPlayer.cursor.trs.gameObject.SetActive(true);
			}
			else if (message.Type == MessageTypes.PLAYER_LEFT)
			{
				OnDestroy ();
				Game.instance.GameOver ();
			}
			else if (message.Type == MessageTypes.UPDATE_UNIT_INFO)
			{
				for (int i = 0; i < Game.enemyPlayer.units.Length; i ++)
				{
					if (i >= (message.Count - 1) / 2)
						return;
					Unit enemyUnit = Game.enemyPlayer.units[i];
					enemyUnit.trs.position = new Vector2(message.GetFloat((uint) i * 2), message.GetFloat((uint) i * 2 + 1));
					enemyUnit.trs.position += (Vector3) enemyUnit.rigid.velocity * GetSecondsSinceAction(message.GetUInt(message.Count - 1));
				}
			}
			else// if (message.Type == MessageTypes.UNIT_DIED)
			{
				Unit enemyUnit = Game.enemyPlayer.units[message.GetUInt(0)];
				enemyUnit.Death ();
			}
		}

		static void ProcessAction (Action action)
		{
			if (action.type == Action.Type.ChangedMovementType)
			{
				Game.enemyPlayer.movementType = (Player.MovementType) Enum.ToObject(typeof(Player.MovementType), action.movementType);
				if (Game.enemyPlayer.movementType == Player.MovementType.PullUnits)
					Game.enemyPlayer.pullMultiplier = 1;
				else if (Game.enemyPlayer.movementType == Player.MovementType.PushUnits)
					Game.enemyPlayer.pullMultiplier = -1;
			}
			else if (action.type == Action.Type.MovedCursor)
				Game.enemyPlayer.cursor.MoveTo (action.cursorPosition);
			else
				enemyPlayerUpdater.usingAbilities = action.usingAbilities;
			for (int i = 0; i < Game.enemyPlayer.units.Length; i ++)
			{
				Unit enemyUnit = Game.enemyPlayer.units[i];
				enemyUnit.trs.position -= (Vector3) enemyUnit.rigid.velocity * GetSecondsSinceAction(action.ticksSinceGameStarted);
				Vector2 move = new Vector2();
				if (Game.enemyPlayer.movementType == Player.MovementType.MoveUnits)
				{
					move = Game.enemyPlayer.cursor.trs.position - Game.enemyPlayer.cursor.previousPositions[0];
					enemyUnit.trs.position += Vector3.ClampMagnitude(move, enemyUnit.moveSpeed) * GetSecondsSinceAction(action.ticksSinceGameStarted);
				}
				else
				{
					move = (Game.enemyPlayer.cursor.trs.position - enemyUnit.trs.position) * Game.enemyPlayer.pullMultiplier;
					enemyUnit.trs.position += (Vector3) move.normalized * enemyUnit.moveSpeed * GetSecondsSinceAction(action.ticksSinceGameStarted);
				}
				enemyUnit.SetFacing (move);
			}
		}

		static float GetSecondsSinceAction (uint ticksSinceGameStartedOfAction)
		{
			long ticksSinceGameStarted = DateTime.UtcNow.Ticks - ticksAtStartOfGame;
			return (float) (ticksSinceGameStarted - ticksSinceGameStartedOfAction) / TimeSpan.TicksPerSecond;
		}

		public static void OnLocalUnitDied (Unit unit)
		{
			NetworkManager.connection.Send(MessageTypes.UNIT_DIED, (uint) Game.localPlayer.units.IndexOf(unit));
		}

		void OnDestroy ()
		{
			SceneManager.sceneLoaded -= OnSceneLoaded;
			if (instance != this)
				return;
			GameManager.updatables = GameManager.updatables.Remove(localPlayerUpdater);
			GameManager.updatables = GameManager.updatables.Remove(enemyPlayerUpdater);
			if (connection != null)
				connection.Disconnect();
		}

		public struct MessageTypes
		{
			public const string PLAYER_MOVED_CURSOR = "Player moved cursor";
			public const string PLAYER_SET_MOVEMENT_TYPE = "Player set movement type";
			public const string PLAYER_TOGGLED_ABILITY_KEY = "Player toggled ability key";
			public const string PLAYER_LEFT = "Player left";
			public const string GAME_STARTED = "Game started";
			public const string UPDATE_UNIT_INFO = "Update unit info";
			public const string UNIT_DIED = "Unit died";
		}

		public struct Action
		{
			public uint ticksSinceGameStarted;
			public Type type;
			public byte movementType;
			public Vector2 cursorPosition;
			public bool[] usingAbilities;

			public Action (byte movementType, uint ticksSinceGameStarted)
			{
				this.ticksSinceGameStarted = ticksSinceGameStarted;
				type = Type.ChangedMovementType;
				this.movementType = movementType;
				cursorPosition = default(Vector2);
				usingAbilities = default(bool[]);
			}

			public Action (Vector2 cursorPosition, uint ticksSinceGameStarted)
			{
				this.ticksSinceGameStarted = ticksSinceGameStarted;
				type = Type.MovedCursor;
				movementType = default(byte);
				this.cursorPosition = cursorPosition;
				usingAbilities = default(bool[]);
			}

			public Action (bool[] usingAbilities, uint ticksSinceGameStarted)
			{
				this.ticksSinceGameStarted = ticksSinceGameStarted;
				type = Type.ToggledAbilityKey;
				movementType = default(byte);
				cursorPosition = default(Vector2);
				this.usingAbilities = usingAbilities;
			}

			public enum Type
			{
				ChangedMovementType,
				MovedCursor,
				ToggledAbilityKey
			}
		}

		public class LocalPlayerUpdater : IUpdatable
		{
			Vector2 previousCursorPosition;
			byte previousMovementType;
			bool[] usingAbilities = new bool[3];
			float updateUnitsTimer;

			public void DoUpdate ()
			{
				if (Game.localPlayer.cursor == null)
					return;
				if ((Vector2) Game.localPlayer.cursor.trs.position != previousCursorPosition)
				{
					NetworkManager.connection.Send(MessageTypes.PLAYER_MOVED_CURSOR, Game.localPlayer.cursor.trs.position.x, Game.localPlayer.cursor.trs.position.y, (uint) (DateTime.UtcNow.Ticks - ticksAtStartOfGame));
					previousCursorPosition = Game.localPlayer.cursor.trs.position;
				}
				byte movementType = (byte) Game.localPlayer.movementType.GetHashCode();
				if (movementType != previousMovementType)
				{
					NetworkManager.connection.Send(MessageTypes.PLAYER_SET_MOVEMENT_TYPE, (uint) movementType.GetHashCode(), (uint) (DateTime.UtcNow.Ticks - ticksAtStartOfGame));
					previousMovementType = movementType;
				}
				bool toggledAbilityKey = false;
				for (int i = 0; i < usingAbilities.Length; i ++)
				{
					bool usingAbility = usingAbilities[i];
					Unit unit = Game.localPlayer.units[0];
					if (unit.abilities.Length > i && usingAbility != unit.abilities[i].shouldUse())
					{
						usingAbilities[i] = !usingAbility;
						toggledAbilityKey = true;
					}
				}
				if (toggledAbilityKey)
					NetworkManager.connection.Send(MessageTypes.PLAYER_TOGGLED_ABILITY_KEY, usingAbilities[0], usingAbilities[1], usingAbilities[2], (uint) (DateTime.UtcNow.Ticks - ticksAtStartOfGame));
				updateUnitsTimer += Time.deltaTime;
				if (updateUnitsTimer > NetworkManager.instance.updateUnitsInterval)
				{
					Message message = Message.Create(MessageTypes.UPDATE_UNIT_INFO);
					for (int i = 0; i < Game.localPlayer.units.Length; i ++)
					{
						Unit unit = Game.localPlayer.units[i];
						message.Add(unit.trs.position.x);
						message.Add(unit.trs.position.y);
					}
					message.Add((uint) (DateTime.UtcNow.Ticks - ticksAtStartOfGame));
					NetworkManager.connection.Send(message);
					updateUnitsTimer -= NetworkManager.instance.updateUnitsInterval;
				}
			}
		}

		public class EnemyPlayerUpdater : IUpdatable
		{
			public bool[] usingAbilities = new bool[3];

			public void DoUpdate ()
			{
				if (actions.Count > 0)
				{
					for (int i = 0; i < actions.Count; i ++)
					{
						Action action = actions[i];
						actions.RemoveAt(i);
						i --;
						ProcessAction (action);
						UpdateUnits ();
					}
				}
				else
					UpdateUnits ();
				Game.enemyPlayer.HealUnits ();
			}

			void UpdateUnits ()
			{
				for (int i = 0; i < Game.enemyPlayer.units.Length; i ++)
				{
					Unit enemyUnit = Game.enemyPlayer.units[i];
					for (int i2 = 0; i2 < enemyUnit.abilities.Length; i2 ++)
					{
						Ability ability = enemyUnit.abilities[i2];
						if (usingAbilities[i2])
							enemyUnit.DoAbility (i2);
					}
				}
				if (Game.enemyPlayer.movementType == Player.MovementType.MoveUnits)
					Game.enemyPlayer.MoveUnits ();
				else
					Game.enemyPlayer.PullUnits ();
			}
		}
	}
}