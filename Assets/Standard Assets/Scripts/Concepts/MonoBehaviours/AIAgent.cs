using System;
using Extensions;
using UnityEngine;
using Unity.MLAgents;
using System.Collections;
using Unity.MLAgents.Sensors;
using Unity.MLAgents.Policies;
using UnityEngine.InputSystem;
using Unity.MLAgents.Actuators;
using System.Collections.Generic;

namespace RTS
{
	public class AIAgent : Agent, IUpdatable
	{
		public SerializableDictionary<byte, float> hpRewardsForUnitTypesDict = new SerializableDictionary<byte, float>();
		public SerializableDictionary<byte, float> deathRewardsForUnitTypesDict = new SerializableDictionary<byte, float>();
		public Player player;
		public SerializableDictionary<byte, UnitGroup> unitTypesForRacesDict = new SerializableDictionary<byte, UnitGroup>();
		public SerializableDictionary<byte, UnitInfo> maxUnitInfoValuesForRacesDict = new SerializableDictionary<byte, UnitInfo>();
		public bool setMaxUnitInfoValuesForRacesDict;
		public BehaviorParameters behaviorParameters;
		public float trainingTimeScale;
		public float offensiveRewardMultiplier;
		public static AIAgent instance;
		static List<float> rewards = new List<float>();
		static float reward;
		static float previousReward;
		static float deltaReward;
		static float totalDeltaReward;
		static float averageTotalDeltaReward;
		static float previousAverageTotalDeltaReward;
		static float averageTotalDeltaRewardVelocity;
		static float rewardVelocity;
		Rect cursorRect;
		bool[] useAbilities = new bool[3];
		int pullMultiplier = 1;
		bool leftClickInput;
		bool previousLeftClickInput;
		bool rightClickInput;
		bool previousRightClickInput;
		const float SNAP_OBSERVATION_VALUES = 0.02f;
		const uint MAX_EXPECTED_UNITS_PER_PLAYER = 24;
		const uint MAX_EXPECTED_BULLETS = MAX_EXPECTED_UNITS_PER_PLAYER * 3;

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (setMaxUnitInfoValuesForRacesDict)
			{
				setMaxUnitInfoValuesForRacesDict = false;
				Game.Instance.Awake ();
				unitTypesForRacesDict.Init();
				for (int i = 0; i < Game.players.Length; i ++)
				{
					Player player = Game.players[i];
					UnitInfo maxUnitInfoValues = new UnitInfo();
					maxUnitInfoValues.playerIndex = (byte) (Game.players.Length - 1);
					maxUnitInfoValues.raceIndex = (byte) (unitTypesForRacesDict.Count - 1);
					Unit[] raceUnitTypes = unitTypesForRacesDict[player.raceIndex].units;
					maxUnitInfoValues.typeIndex = (byte) Mathf.Max(maxUnitInfoValues.typeIndex, raceUnitTypes.Length - 1);
					for (int i2 = 0; i2 < raceUnitTypes.Length; i2 ++)
					{
						Unit unit = raceUnitTypes[i2];
						maxUnitInfoValues.position = Vector2.one;
						maxUnitInfoValues.hp = Mathf.Max(maxUnitInfoValues.hp, unit.maxHp);
						maxUnitInfoValues.moveSpeed = Mathf.Max(maxUnitInfoValues.moveSpeed, unit.moveSpeed);
						maxUnitInfoValues.abilitiesCooldownTimeRemaining = new float[unit.abilities.Length];
						maxUnitInfoValues.abilitiesChannelTimeRemaining = new float[unit.abilities.Length];
						for (int i3 = 0; i3 < unit.abilities.Length; i3 ++)
						{
							Ability ability = unit.abilities[i3];
							maxUnitInfoValues.abilitiesCooldownTimeRemaining[i3] = Mathf.Max(maxUnitInfoValues.abilitiesCooldownTimeRemaining[i3], ability.cooldown);
							maxUnitInfoValues.abilitiesChannelTimeRemaining[i3] = Mathf.Max(maxUnitInfoValues.abilitiesChannelTimeRemaining[i3], ability.maxChannelTime);
						}
					}
					maxUnitInfoValuesForRacesDict[player.raceIndex] = maxUnitInfoValues;
				}
			}
		}
#endif

		public override void Initialize ()
		{
			if (!Academy.Instance.IsCommunicatorOn)
			{
				reward = 0;
				totalDeltaReward = 0;
				deltaReward = 0;
				averageTotalDeltaReward = 0;
				previousAverageTotalDeltaReward = 0;
				averageTotalDeltaRewardVelocity = 0;
				previousReward = 0;
				rewardVelocity = 0;
				MaxStep = 0;
			}
			maxUnitInfoValuesForRacesDict.Init ();
			cursorRect = CameraScript.Instance.viewRect;
			instance = this;
			hpRewardsForUnitTypesDict.Init ();
			deathRewardsForUnitTypesDict.Init ();
			if (behaviorParameters.BehaviorType == BehaviorType.HeuristicOnly)
				Time.timeScale = trainingTimeScale;
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void OnDisable ()
		{
			base.OnDisable ();
			reward = 0;
			previousReward = 0;
			rewards.Clear();
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		public void DoUpdate ()
		{
			EndEpisode();
			Academy.Instance.EnvironmentStep();
			reward = GetCumulativeReward();
			print("Reward: " + reward);
			rewards.Add(reward);
			deltaReward = reward - previousReward;
			print("Delta reward: " + deltaReward);
			totalDeltaReward += deltaReward;
			print("Total delta reward: " + totalDeltaReward);
			averageTotalDeltaReward = totalDeltaReward / rewards.Count;
			print("Average total delta reward: " + averageTotalDeltaReward);
			// print("Reward velocity: " + rewardVelocity);
			previousReward = reward;
		}

		public override void CollectObservations (VectorSensor sensor)
		{
			for (int i = 0; i < Game.players.Length; i ++)
			{
				Player player = Game.players[i];
				for (int i2 = 0; i2 < MAX_EXPECTED_UNITS_PER_PLAYER; i2 ++)
				{
					if (player.units.Length > i2)
					{
						Unit unit = player.units[i2];
						UnitInfo maxUnitInfoValues = maxUnitInfoValuesForRacesDict[player.raceIndex];
						if (unit.trs != null)
						{
							sensor.AddObservation(Mathf.InverseLerp(0, maxUnitInfoValues.playerIndex, i));
							sensor.AddObservation(Mathf.InverseLerp(0, maxUnitInfoValues.typeIndex, unit.type));
							Vector2 normalizedPosition = Rect.PointToNormalized(cursorRect, unit.trs.position);
							sensor.AddObservation(MathfExtensions.SnapToInterval(normalizedPosition.x, SNAP_OBSERVATION_VALUES));
							sensor.AddObservation(MathfExtensions.SnapToInterval(normalizedPosition.y, SNAP_OBSERVATION_VALUES));
							sensor.AddObservation(MathfExtensions.SnapToInterval(Mathf.InverseLerp(0, maxUnitInfoValues.hp, unit.maxHp), SNAP_OBSERVATION_VALUES));
							Ability ability = unit.abilities[0];
							sensor.AddObservation(MathfExtensions.SnapToInterval(Mathf.InverseLerp(0, ability.cooldown, ability.cooldownTimeRemaining), SNAP_OBSERVATION_VALUES));
							ability = unit.abilities[1];
							sensor.AddObservation(MathfExtensions.SnapToInterval(Mathf.InverseLerp(0, ability.cooldown, ability.cooldownTimeRemaining), SNAP_OBSERVATION_VALUES));
							ability = unit.abilities[2];
							sensor.AddObservation(MathfExtensions.SnapToInterval(Mathf.InverseLerp(0, ability.cooldown, ability.cooldownTimeRemaining), SNAP_OBSERVATION_VALUES));
						}
						else
						{
							sensor.AddObservation(0);
							sensor.AddObservation(0);
							sensor.AddObservation(0);
							sensor.AddObservation(0);
							sensor.AddObservation(0);
							sensor.AddObservation(0);
							sensor.AddObservation(0);
							sensor.AddObservation(0);
						}
					}
					else
					{
						sensor.AddObservation(0);
						sensor.AddObservation(0);
						sensor.AddObservation(0);
						sensor.AddObservation(0);
						sensor.AddObservation(0);
						sensor.AddObservation(0);
						sensor.AddObservation(0);
						sensor.AddObservation(0);
					}
				}
			}
			for (int i = 0; i < MAX_EXPECTED_BULLETS; i ++)
			{
				if (Bullet.bullets.Count > i)
				{
					Bullet bullet = Bullet.bullets[i];
					Unit shooter = bullet.shooter;
					if (shooter.player == this)
					{
						UnitInfo maxUnitInfoValues = maxUnitInfoValuesForRacesDict[shooter.player.raceIndex];
						Vector2 normalizedPosition = Rect.PointToNormalized(cursorRect, bullet.trs.position);
						sensor.AddObservation(MathfExtensions.SnapToInterval(normalizedPosition.x, SNAP_OBSERVATION_VALUES));
						sensor.AddObservation(MathfExtensions.SnapToInterval(normalizedPosition.y, SNAP_OBSERVATION_VALUES));
						sensor.AddObservation(Mathf.InverseLerp(0, maxUnitInfoValues.typeIndex, shooter.type));
					}
					else
					{
						sensor.AddObservation(0);
						sensor.AddObservation(0);
						sensor.AddObservation(0);
					}
				}
				else
				{
					sensor.AddObservation(0);
					sensor.AddObservation(0);
					sensor.AddObservation(0);
				}
			}
		}

		public override void OnActionReceived (ActionBuffers actionBuffers)
		{
			if (behaviorParameters.BehaviorType == BehaviorType.HeuristicOnly)
				return;
			ActionSegment<float> continuousActions = actionBuffers.ContinuousActions;
			AIPlayer.Action action = new AIPlayer.Action();
			float normalizedCursorXPosition = ScaleAction(continuousActions[0], 0, 1);
			float normalizedCursorYPosition = ScaleAction(continuousActions[1], 0, 1);
			action.cursorPosition = cursorRect.min + cursorRect.size.Multiply(new Vector2(normalizedCursorXPosition, normalizedCursorYPosition));
			action.useAbilities = new bool[3];
			for (int i = 0; i < 3; i ++)
				useAbilities[i] = continuousActions[ + 2] > 0;
			float scaledContinousActionForMovementType = ScaleAction(continuousActions[5], 0, 3);
			action.movementType = (Player.MovementType) Enum.ToObject(typeof(Player.MovementType), (byte) scaledContinousActionForMovementType);
			AIPlayer aiPlayer = player as AIPlayer;
			if (aiPlayer != null)
				aiPlayer.currentAction = action;
		}

		public override void Heuristic (in ActionBuffers actionBuffers)
		{
			ActionSegment<float> continuousActions = actionBuffers.ContinuousActions;
			AIPlayer.Action action = new AIPlayer.Action();
			action.cursorPosition = CameraScript.instance.camera.ScreenToWorldPoint((Vector2) InputManager.MousePosition);
			action.useAbilities = new bool[3];
			action.useAbilities[0] = Keyboard.current.digit1Key.isPressed;
			action.useAbilities[1] = Keyboard.current.digit2Key.isPressed;
			action.useAbilities[2] = Keyboard.current.digit3Key.isPressed;
			leftClickInput = InputManager.LeftClickInput;
			rightClickInput = InputManager.RightClickInput;
			if (leftClickInput && !previousLeftClickInput)
			{
				pullMultiplier *= -1;
				if (pullMultiplier > 0)
					action.movementType = Player.MovementType.PullUnits;
				else
					action.movementType = Player.MovementType.PushUnits;
			}
			if (rightClickInput)
				action.movementType = Player.MovementType.MoveUnits;
			AIPlayer aiPlayer = player as AIPlayer;
			if (aiPlayer != null)
				aiPlayer.currentAction = action;
			previousLeftClickInput = leftClickInput;
			previousRightClickInput = rightClickInput;
			Vector2 normalizedCursorPosition = Rect.PointToNormalized(cursorRect, action.cursorPosition);
			continuousActions[0] = normalizedCursorPosition.x;
			continuousActions[1] = normalizedCursorPosition.y;
			continuousActions[2] = useAbilities[0].GetHashCode();
			continuousActions[3] = useAbilities[1].GetHashCode();
			continuousActions[4] = useAbilities[2].GetHashCode();
			continuousActions[5] = (float) action.movementType.GetHashCode() / 3;
		}

		public void AddHealReward (Unit unit, float amount)
		{
			int rewardMultiplier = 1;
			if (unit.player != player)
				rewardMultiplier = -1;
			AddReward(hpRewardsForUnitTypesDict[unit.type] * amount * rewardMultiplier);
		}

		public void AddDamageReward (Unit unit, float amount)
		{
			int rewardMultiplier = 1;
			if (unit.player != player)
				rewardMultiplier = -1;
			AddReward(-hpRewardsForUnitTypesDict[unit.type] * amount * rewardMultiplier);
		}

		public void AddDeathReward (Unit unit)
		{
			int rewardMultiplier = 1;
			if (unit.player != player)
				rewardMultiplier = -1;
			AddReward(-deathRewardsForUnitTypesDict[unit.type] * rewardMultiplier);
		}

		[Serializable]
		public struct UnitInfo
		{
			public byte playerIndex;
			public byte raceIndex;
			public byte typeIndex;
			public Vector2 position;
			public float hp;
			public float moveSpeed;
			public float[] abilitiesCooldownTimeRemaining;
			public float[] abilitiesChannelTimeRemaining;
		}

		[Serializable]
		public class UnitGroup
		{
			public Unit[] units;
		}
	}
}