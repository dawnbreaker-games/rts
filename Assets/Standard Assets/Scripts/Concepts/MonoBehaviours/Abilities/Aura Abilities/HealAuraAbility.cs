using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace RTS
{
	public class HealAuraAbility : AuraAbility
	{
		public bool applyOverTime;
		public float amount;

		public override void Do ()
		{
			if (!isCoolingDown && !doAnimation)
				ApplyHealAura ();
			base.Do ();
		}
		
		public void ApplyHealAura ()
		{
			if (!applyOverTime)
				AffectUnits ((Unit unit) => { unit.Heal (amount); });
			else
				StartCoroutine (AuraRoutine ());
		}

		IEnumerator AuraRoutine ()
		{
			float startTime = Time.time;
			while (true)
			{
				AffectUnits ((Unit unit) => { unit.Heal (amount * Time.deltaTime); });
				yield return new WaitForEndOfFrame();
				if (Time.time - startTime > duration)
					break;
			}
		}
	}
}