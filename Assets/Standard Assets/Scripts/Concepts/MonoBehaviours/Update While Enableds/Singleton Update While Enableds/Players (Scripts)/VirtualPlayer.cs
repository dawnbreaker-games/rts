using System;
using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;
using UnityEngine.InputSystem.Controls;
using Random = UnityEngine.Random;

namespace RTS
{
	public class VirtualPlayer : Player
	{
		public float[] considerFutureTimes = new float[0];
		public float similarActionValueThreshold;
		public Vector2Int cursorRectCellCount;
		public float maxFractionOfCursorRectCellSizeToOffset;
		public float maxFractionOfPossibleActionsNotLeadingToOffenseToEnemy;
		public int minPossibleActionsConsideredToDecideIfNotOffensiveEnough;
		public float minDistanceToAverageEnemyPositionTillTargetClosestEnemyUnit;
		public SerializableDictionary<byte, float> hpWeightsForUnitTypesDict = new SerializableDictionary<byte, float>();
		public SerializableDictionary<byte, float> deathWeightsForUnitTypesDict = new SerializableDictionary<byte, float>();
		public int[] offensiveAbilitiesToEnemy = new int[0];
		public int[][] offensiveAbilitiesCombinationsToEnemy;
		public int[][] UseAbilitiesCombinations
		{
			get
			{
				int[][] output = new int[8][];
				output[0] = new int[0];
				output[1] = new int[3] { 0, 1, 2 };
				output[2] = new int[1] { 0 };
				output[3] = new int[1] { 1 };
				output[4] = new int[1] { 2 };
				output[5] = new int[2] { 0, 1 };
				output[6] = new int[2] { 0, 2 };
				output[7] = new int[2] { 1, 2 };
				return output;
			}
		}
		Rect cursorRect;
		Rect cursorPositionOffsetRect;
		Vector2 cursorRectCellSize;
		Action currentAction;
		int movementTypeCount;
		int possibleActionsNotLeadingToOffenseToEnemyCount;
		float fractionOfPossibleActionsNotLeadingToOffenseToEnemy;
		AbilityEvent[] eventsToHappen = new AbilityEvent[0];

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				base.OnEnable ();
				return;
			}
#endif
			List<int[]> offensiveAbilitiesCombinationsToEnemy = new List<int[]>();
			for (int i = 0; i < UseAbilitiesCombinations.Length; i ++)
			{
				int[] useAbilitiesCombination = UseAbilitiesCombinations[i];
				for (int i2 = 0; i2 < offensiveAbilitiesToEnemy.Length; i2 ++)
				{
					int abilityIndex = offensiveAbilitiesToEnemy[i2];
					if (useAbilitiesCombination.Contains(abilityIndex))
					{
						offensiveAbilitiesCombinationsToEnemy.Add(useAbilitiesCombination);
						break;
					}
				}
			}
			this.offensiveAbilitiesCombinationsToEnemy = offensiveAbilitiesCombinationsToEnemy.ToArray();
			movementTypeCount = Enum.GetNames(typeof(MovementType)).Length;
			hpWeightsForUnitTypesDict.Init ();
			deathWeightsForUnitTypesDict.Init ();
			cursorRect = CameraScript.Instance.viewRect;
			cursorRectCellSize = cursorRect.size / cursorRectCellCount;
			cursorPositionOffsetRect = Rect.MinMaxRect(-cursorRectCellSize.x / 2 * maxFractionOfCursorRectCellSizeToOffset, -cursorRectCellSize.y / 2 * maxFractionOfCursorRectCellSizeToOffset, cursorRectCellSize.x / 2 * maxFractionOfCursorRectCellSizeToOffset, cursorRectCellSize.y / 2 * maxFractionOfCursorRectCellSizeToOffset);
			for (int i = 0; i < units.Length; i ++)
			{
				Unit unit = units[i];
				OnGainedUnit (unit);
			}
			for (int i = 0; i < buildings.Length; i ++)
			{
				Building building = buildings[i];
				building.Init ();
			}
			if (Instance != this && handleMultipleInstances != MultipleInstancesHandlingType.KeepAll)
			{
				if (handleMultipleInstances == MultipleInstancesHandlingType.DestroyNew)
				{
					Destroy(gameObject);
					return;
				}
				else
					Destroy(instance.gameObject);
			}
			if (persistant)
				DontDestroyOnLoad(gameObject);
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public override void DoUpdate ()
		{
			Think ();
			Act ();
		}

		void Think ()
		{
			FloatRange timeRange = new FloatRange(Time.time + considerFutureTimes[0], Time.time + considerFutureTimes[1]);
			GameState gameState = GetGameState();
			for (int i = 0; i < eventsToHappen.Length; i ++)
			{
				AbilityEvent abilityEvent = eventsToHappen[i];
				if (Time.time > abilityEvent.time && (abilityEvent.mustHappenOnTime || !currentAction.useAbilities.Contains(abilityEvent.abilityIndex)))
				{
					abilityEvent.actionOnUnits (abilityEvent.unitInfos);
					eventsToHappen = eventsToHappen.RemoveAt(i);
					i --;
				}
			}
			Action[] predictedActions = ConsiderActions(timeRange, gameState);
			List<TreeNode<Action>> actionNodesRemaining = new List<TreeNode<Action>>();
			float maxPredictedActionValue = -Mathf.Infinity;
			List<TreeNode<Action>> bestPredictedActionNodes = new List<TreeNode<Action>>();
			for (int i = 0; i < predictedActions.Length; i ++)
			{
				Action predictedAction = predictedActions[i];
				actionNodesRemaining.Add(new TreeNode<Action>(predictedAction));
			}
			while (actionNodesRemaining.Count > 0)
			{
				TreeNode<Action> actionNode = actionNodesRemaining[0];
				if (actionNode.parent != null)
					currentAction.value += actionNode.parent.value.value;
				int futureTimeIndex = actionNode.value.indexOfStartTime + 1;
				if (futureTimeIndex < considerFutureTimes.Length - 1)
				{
					float predictedActionStartTime = Time.time + considerFutureTimes[futureTimeIndex];
					timeRange = new FloatRange(predictedActionStartTime, Time.time + considerFutureTimes[futureTimeIndex + 1]);
					if (currentAction.gameStateAfterAction.Equals(default(GameState)))
						actionNodesRemaining.AddRange(actionNode.AddChildren(ConsiderActions(timeRange, gameState)));
					else
						actionNodesRemaining.AddRange(actionNode.AddChildren(ConsiderActions(timeRange, currentAction.gameStateAfterAction)));
				}
				else
				{
					float actionValue = currentAction.value;
					if (actionValue >= maxPredictedActionValue - similarActionValueThreshold)
					{
						maxPredictedActionValue = actionValue;
						for (int i = 0; i < bestPredictedActionNodes.Count; i ++)
						{
							TreeNode<Action> bestPredictedActionNode = bestPredictedActionNodes[i];
							if (bestPredictedActionNode.value.value < maxPredictedActionValue - similarActionValueThreshold)
							{
								bestPredictedActionNodes.RemoveAt(i);
								i --;
							}
						}
						bestPredictedActionNodes.Add(actionNode);
					}
				}
				actionNodesRemaining.RemoveAt(0);
			}
			currentAction = bestPredictedActionNodes[Random.Range(0, bestPredictedActionNodes.Count)].GetRoot().value;
			eventsToHappen = currentAction.gameStateAfterAction.eventsToHappen;
		}

		Action[] ConsiderActions (FloatRange timeRange, GameState previousGameState)
		{
			// Action[] output = new Action[cursorRectCellCount.x * cursorRectCellCount.y * 8 * movementTypeCount];
			// int actionIndex = 0;
			List<Action> output = new List<Action>();
			for (float positionX = cursorRect.xMin + cursorRectCellSize.x / 2; positionX <= cursorRect.xMax - cursorRectCellSize.x / 2; positionX += cursorRectCellSize.x)
			{
				for (float positionY = cursorRect.yMin + cursorRectCellSize.y / 2; positionY <= cursorRect.yMax - cursorRectCellSize.y / 2; positionY += cursorRectCellSize.y)
				{
					Vector2 position = new Vector2(positionX, positionY) + cursorPositionOffsetRect.RandomPoint();
					for (int i = 0; i < UseAbilitiesCombinations.Length; i ++)
					{
						int[] abilityUseCombination = UseAbilitiesCombinations[i];
						for (int i2 = 0; i2 < movementTypeCount; i2 ++)
						{
							Action action = new Action();
							for (int i3 = 1; i3 < considerFutureTimes.Length - 1; i3 ++)
							{
								float considerFutureTime = considerFutureTimes[i3];
								if (Time.time + considerFutureTime > timeRange.min)
								{
									action.indexOfStartTime = i3 - 1;
									break;
								}
							}
							MovementType movementType = (MovementType) Enum.ToObject(typeof(MovementType), i2);
							action.movementType = movementType;
							action.cursorPosition = position;
							action.useAbilities = abilityUseCombination;
							cursor.MoveTo (position);
							GameState gameState = PredictGameState(previousGameState, abilityUseCombination, timeRange.max - timeRange.min);
							action.gameStateAfterAction = gameState;
							action.value = gameState.myTotalWeightedHp - previousGameState.myTotalWeightedHp - (gameState.enemyTotalWeightedHp - previousGameState.enemyTotalWeightedHp);
							action.isOffensiveToEnemy = previousGameState.enemyTotalWeightedHp - gameState.enemyTotalWeightedHp > 0;
							if (!action.isOffensiveToEnemy)
							{
								possibleActionsNotLeadingToOffenseToEnemyCount ++;
								if (possibleActionsNotLeadingToOffenseToEnemyCount >= minPossibleActionsConsideredToDecideIfNotOffensiveEnough)
								{
									fractionOfPossibleActionsNotLeadingToOffenseToEnemy = (float) possibleActionsNotLeadingToOffenseToEnemyCount / output.Count;
									if (fractionOfPossibleActionsNotLeadingToOffenseToEnemy >= maxFractionOfPossibleActionsNotLeadingToOffenseToEnemy)
										return GetOffensiveActionsToEnemy(timeRange);
								}
							}
							List<Unit> deadUnits = new List<Unit>();
							for (int i3 = 0; i3 < previousGameState.unitInfos.Length; i3 ++)
							{
								GameState.UnitInfo previousUnitInfo = previousGameState.unitInfos[i3];
								deadUnits.Add(previousUnitInfo.unit);
							}
							for (int i3 = 0; i3 < gameState.unitInfos.Length; i3 ++)
							{
								GameState.UnitInfo unitInfo = gameState.unitInfos[i3];
								deadUnits.Remove(unitInfo.unit);
							}
							for (int i3 = 0; i3 < deadUnits.Count; i3 ++)
							{
								Unit deadUnit = deadUnits[i3];
								if (deadUnit.player == this)
									action.value -= deathWeightsForUnitTypesDict[deadUnit.type];
								else
									action.value += deathWeightsForUnitTypesDict[deadUnit.type];
							}
							// output[actionIndex] = action;
							// actionIndex ++;
							output.Add(action);
						}
					}
				}
			}
			// return output;
			return output.ToArray();
		}

		GameState GetGameState ()
		{
			GameState output = new GameState();
			output.eventsToHappen = new AbilityEvent[0];
			List<GameState.UnitInfo> unitInfos = new List<GameState.UnitInfo>();
			output.bulletInfos = new GameState.BulletInfo[Bullet.bullets.Count];
			for (int i = 0; i < Game.players.Length; i ++)
			{
				Player player = Game.players[i];
				for (int i2 = 0; i2 < player.units.Length; i2 ++)
				{
					Unit unit = player.units[i2];
					GameState.UnitInfo unitInfo = new GameState.UnitInfo();
					unitInfo.unit = unit;
					// unitInfo.timeRange = new FloatRange(unit.spawnTime, Mathf.Infinity);
					unitInfo.position = unit.trs.position;
					unitInfo.hp = unit.Hp;
					unitInfo.moveSpeed = unit.moveSpeed;
					unitInfo.abilitiesCooldownTimeRemaining = new float[unit.abilities.Length];
					for (int i3 = 0; i3 < unit.abilities.Length; i3 ++)
					{
						Ability ability = unit.abilities[i3];
						unitInfo.abilitiesCooldownTimeRemaining[i3] = ability.cooldownTimeRemaining;
					}
					unitInfos.Add(unitInfo);
				}
			}
			output.unitInfos = unitInfos.ToArray();
			for (int i = 0; i < Bullet.bullets.Count; i ++)
			{
				Bullet bullet = Bullet.bullets[i];
				GameState.BulletInfo bulletInfo = new GameState.BulletInfo();
				bulletInfo.bullet = bullet;
				bulletInfo.timeRange = new FloatRange(bullet.spawnTime, bullet.spawnTime + bullet.lifetime);
				bulletInfo.position = bullet.trs.position;
				bulletInfo.facing = bullet.trs.up;
				bulletInfo.hitsBeforeDespawnRemaining = bullet.hitsBeforeDespawnRemaining;
				bulletInfo.damageMultiplier = bullet.damageMultiplier;
				output.bulletInfos[i] = bulletInfo;
			}
			return output;
		}

		GameState PredictGameState (GameState currentGameState, int[] useAbilities, float timeToNewGameState)
		{
			GameState output = currentGameState;
			for (int i = 0; i < output.unitInfos.Length; i ++)
			{
				GameState.UnitInfo unitInfo = output.unitInfos[i];
				Unit unit = unitInfo.unit;
				for (int i3 = 0; i3 < unit.abilities.Length; i3 ++)
				{
					Ability ability = unit.abilities[i3];
					float abilityCooldownTimeRemaining = unitInfo.abilitiesCooldownTimeRemaining[i3];
					if (useAbilities.Contains(i3))
					{
						float timeToNewGameStateRemaining = timeToNewGameState;
						while (timeToNewGameStateRemaining > 0)
						{
							float timePassed = timeToNewGameStateRemaining;
							if (ability.cooldown > 0)
								timePassed = Mathf.Min(ability.cooldown, timeToNewGameStateRemaining);
							timeToNewGameStateRemaining -= timePassed;
							abilityCooldownTimeRemaining -= timePassed;
							if (abilityCooldownTimeRemaining <= 0)
							{
								abilityCooldownTimeRemaining = ability.cooldown;
								if (timeToNewGameStateRemaining > 0)
									output = PredictDoAbilityOutcome(ability, i3, ref unitInfo, output, Time.time + (timeToNewGameState - timeToNewGameStateRemaining), timeToNewGameStateRemaining);
							}
						}
					}
					else if (abilityCooldownTimeRemaining >= 0)
					{
						abilityCooldownTimeRemaining -= timeToNewGameState;
						if (abilityCooldownTimeRemaining < 0)
							abilityCooldownTimeRemaining = 0;
					}
					unitInfo.abilitiesCooldownTimeRemaining[i3] = abilityCooldownTimeRemaining;
				}
				output.unitInfos[i] = unitInfo;
				if (unit.player == this)
					output.myTotalWeightedHp -= unit.Hp * hpWeightsForUnitTypesDict[unit.type];
				else
					output.enemyTotalWeightedHp -= unit.Hp * hpWeightsForUnitTypesDict[unit.type];
			}
			List<Vector2> newUnitPositions = SetUnitPositions(ref output, timeToNewGameState);
			if (newUnitPositions.Count == 0)
			{
				for (int i = 0; i < output.unitInfos.Length; i ++)
				{
					GameState.UnitInfo unitInfo = output.unitInfos[i];
					unitInfo.position = GetUnitMovementDestination(unitInfo, timeToNewGameState);
					output.unitInfos[i] = unitInfo;
				}
			}
			else if (output.unitInfos.Length == newUnitPositions.Count)
			{
				for (int i = 0; i < output.unitInfos.Length; i ++)
				{
					GameState.UnitInfo unitInfo = output.unitInfos[i];
					unitInfo.position = newUnitPositions[i];
					output.unitInfos[i] = unitInfo;
				}
			}
			return output;
		}

		List<Vector2> SetUnitPositions (ref GameState gameState, float timeToNewGameState)
		{
			List<Vector2> output = new List<Vector2>();
			for (int i = 0; i < gameState.bulletInfos.Length; i ++)
			{
				GameState.BulletInfo bulletInfo = gameState.bulletInfos[i];
				Vector2 newBulletPosition = new Vector2();
				for (int i2 = 0; i2 < gameState.unitInfos.Length; i2 ++)
				{
					GameState.UnitInfo unitInfo = gameState.unitInfos[i2];
					(float? time, Vector2 newUnitPosition, Vector2 newBulletPosition) hitInfo = GetIntersectionInfo(unitInfo, bulletInfo, timeToNewGameState);
					if (hitInfo.time != null)
					{
						unitInfo.hp -= bulletInfo.bullet.damageAmountOnHitDestructable * bulletInfo.damageMultiplier;
						if (unitInfo.hp <= 0)
						{
							gameState.unitInfos = gameState.unitInfos.RemoveAt(i2);
							i2 --;
						}
						else
						{
							output.Add(hitInfo.newUnitPosition);
							gameState.unitInfos[i2] = unitInfo;
						}
						bulletInfo.hitsBeforeDespawnRemaining --;
						if (bulletInfo.hitsBeforeDespawnRemaining == 0)
						{
							gameState.bulletInfos = gameState.bulletInfos.RemoveAt(i);
							i --;
							break;
						}
						newBulletPosition = hitInfo.newBulletPosition;
					}
				}
				if (bulletInfo.hitsBeforeDespawnRemaining > 0)
				{
					bulletInfo.position = newBulletPosition;
					gameState.bulletInfos[i] = bulletInfo;
				}
			}
			return output;
		}

		Action[] GetOffensiveActionsToEnemy (FloatRange timeRange)
		{
			List<Action> output = new List<Action>();
			List<Unit> allEnemyUnitsAndBuildings = new List<Unit>();
			for (int i = 0; i < enemyPlayers.Length; i ++)
			{
				Player player = enemyPlayers[i];
				allEnemyUnitsAndBuildings.AddRange(player.units);
				allEnemyUnitsAndBuildings.AddRange(player.buildings);
			}
			Transform[] enemyUnitTransforms = new Transform[allEnemyUnitsAndBuildings.Count];
			Vector2[] enemyUnitPositions = new Vector2[allEnemyUnitsAndBuildings.Count];
			for (int i = 0; i < allEnemyUnitsAndBuildings.Count; i ++)
			{
				Unit enemyUnit = allEnemyUnitsAndBuildings[i];
				Transform enemyTrs = enemyUnit.trs;
				enemyUnitTransforms[i] = enemyTrs;
				enemyUnitPositions[i] = enemyTrs.position;
			}
			Vector2 averageEnemyPosition = VectorExtensions.GetAverage(enemyUnitPositions);
			Action action = new Action();
			for (int i3 = 1; i3 < considerFutureTimes.Length - 1; i3 ++)
			{
				float considerFutureTime = considerFutureTimes[i3];
				if (Time.time + considerFutureTime > timeRange.min)
				{
					action.indexOfStartTime = i3 - 1;
					break;
				}
			}
			action.isOffensiveToEnemy = true;
			(Transform trs, float distanceSqr) closestEnemyTrsAndDistanceSqr = TransformExtensions.GetClosestTransformAndDistanceSqr_2D(enemyUnitTransforms, averageEnemyPosition);
			if (closestEnemyTrsAndDistanceSqr.distanceSqr < minDistanceToAverageEnemyPositionTillTargetClosestEnemyUnit * minDistanceToAverageEnemyPositionTillTargetClosestEnemyUnit)
				action.cursorPosition = averageEnemyPosition;
			else
				action.cursorPosition = closestEnemyTrsAndDistanceSqr.trs.position;
			action.cursorPosition = averageEnemyPosition;
			action.movementType = MovementType.PullUnits;
			for (int i = 0; i < offensiveAbilitiesCombinationsToEnemy.Length; i ++)
			{
				int[] offensiveAbilitiesCombination = offensiveAbilitiesCombinationsToEnemy[i];
				action.useAbilities = offensiveAbilitiesCombination;
				output.Add(action);
			}
			return output.ToArray();
		}

		GameState PredictDoAbilityOutcome (Ability ability, int abilityIndex, ref GameState.UnitInfo casterInfo, GameState gameState, float timeOfGameState, float timeToNewGameState)
		{
			GameState output = gameState;
			if (ability.doAnimation)
			{
				float animationEventTime = 0;
				AnimationClip animationClip = null;
				Animator animator = ability.animationEntry.animator;
				for (int i = 0; i <  animator.runtimeAnimatorController.animationClips.Length; i ++)
				{
					animationClip = animator.runtimeAnimatorController.animationClips[i];
					if (animationClip.name == name)
						break;
				}
				if (animationClip.events.Length > 0)
				{
					animationEventTime = timeOfGameState + animationClip.events[0].time;
					timeToNewGameState -= animationEventTime - timeOfGameState;
					if (timeToNewGameState > 0)
						output = PredictDoAbilityOutcome(ability, abilityIndex, ref casterInfo, gameState, animationEventTime, timeToNewGameState);
				}
			}
			else
			{
				StopAbilityCooldownAbility stopAbilityCooldownAbility = ability as StopAbilityCooldownAbility;
				if (stopAbilityCooldownAbility != null)
				{
					int abilityIndex2 = casterInfo.unit.abilities.IndexOf(stopAbilityCooldownAbility.ability);
					casterInfo.abilitiesCooldownTimeRemaining[abilityIndex2] = 0;
				}
				else
				{
					SetSelfMoveSpeedAbility setSelfMoveSpeedAbility = ability as SetSelfMoveSpeedAbility;
					if (setSelfMoveSpeedAbility != null)
						casterInfo.moveSpeed = setSelfMoveSpeedAbility.moveSpeed;
					else
					{
						HealSelfAbility healSelfAbility = ability as HealSelfAbility;
						if (healSelfAbility != null)
						{
							float previousHp = casterInfo.hp;
							casterInfo.hp += healSelfAbility.amount;
							Unit unit = casterInfo.unit;
							if (casterInfo.hp > unit.maxHp)
								casterInfo.hp = unit.maxHp;
							if (unit.player = this)
								output.myTotalWeightedHp += (casterInfo.hp - previousHp) * hpWeightsForUnitTypesDict[unit.type];
							else
								output.enemyTotalWeightedHp += (casterInfo.hp - previousHp) * hpWeightsForUnitTypesDict[unit.type];
						}
						else
						{
							CompoundAbility compoundAbility = ability as CompoundAbility;
							if (compoundAbility != null)
							{
								for (int i = 0; i < compoundAbility.abilities.Length; i ++)
								{
									Ability ability2 = compoundAbility.abilities[i];
									output = PredictDoAbilityOutcome(ability2, abilityIndex, ref casterInfo, output, timeOfGameState, timeToNewGameState);
								}
							}
							else
							{
								BulletPatternAbility bulletPatternAbility = ability as BulletPatternAbility;
								if (bulletPatternAbility != null)
									output = PredictShootBulletPatternEntryOutcome(bulletPatternAbility, casterInfo, output, timeOfGameState, timeToNewGameState);
								else
								{
									MultiplyMoveSpeedAuraAbility multiplyMoveSpeedAuraAbility = ability as MultiplyMoveSpeedAuraAbility;
									if (multiplyMoveSpeedAuraAbility != null)
									{
										GameState.UnitInfo[] unitsToAffect = GetUnitsToAffectByAura(gameState, casterInfo, multiplyMoveSpeedAuraAbility);
										for (int i = 0; i < unitsToAffect.Length; i ++)
										{
											GameState.UnitInfo unitInfo = unitsToAffect[i];
											unitInfo.moveSpeed *= multiplyMoveSpeedAuraAbility.multiplyMoveSpeed;
										}
										if (multiplyMoveSpeedAuraAbility.IsChannellable)
										{
											float timeOfEvent = timeOfGameState + Mathf.Min(multiplyMoveSpeedAuraAbility.maxChannelTime, timeToNewGameState);
											output.eventsToHappen = output.eventsToHappen.Add(new AbilityEvent
											(
												abilityIndex,
												unitsToAffect, 
												(GameState.UnitInfo[] unitInfos) =>
												{ 
													for (int i = 0; i < unitInfos.Length; i ++)
													{
														GameState.UnitInfo unitInfo = unitInfos[i];
														unitInfo.moveSpeed /= multiplyMoveSpeedAuraAbility.multiplyMoveSpeed;
													}
												},
												timeOfEvent,
												timeOfEvent != timeOfGameState + timeToNewGameState
											));
										}
									}
									else
									{
										
									}
								}
							}
						}
					}
				}
			}
			return output;
		}

		GameState PredictShootBulletPatternEntryOutcome (BulletPatternAbility bulletPatternAbility, GameState.UnitInfo shooterInfo, GameState gameState, float timeOfGameState, float timeToNewGameState)
		{
			BulletPatternEntry bulletPatternEntry = bulletPatternAbility.bulletPatternEntry;
			GameState output = gameState;
			List<GameState.BulletInfo> bulletInfos = new List<GameState.BulletInfo>(output.bulletInfos);
			BulletPattern bulletPattern = bulletPatternEntry.bulletPattern;
			RepeatBulletPatterns repeatBulletPatterns = bulletPattern as RepeatBulletPatterns;
			if (repeatBulletPatterns != null)
			{
				for (int i = 0; i < repeatBulletPatterns.repeatCount; i ++)
				{
					for (int i2 = 0; i2 < repeatBulletPatterns.bulletPatterns.Length; i2 ++)
					{
						BulletPattern bulletPattern2 = repeatBulletPatterns.bulletPatterns[i2];
						bulletPatternAbility.bulletPatternEntry.bulletPattern = bulletPattern2;
						output = PredictShootBulletPatternEntryOutcome(bulletPatternAbility, shooterInfo, output, timeOfGameState, timeToNewGameState);
						bulletPatternAbility.bulletPatternEntry.bulletPattern = bulletPattern;
					}
				}
			}
			else
			{
				AimWhereFacing aimWhereFacing = bulletPattern as AimWhereFacing;
				if (aimWhereFacing != null)
				{
					GameState.BulletInfo bulletInfo = new GameState.BulletInfo();
					KeyValuePair<GameObject, Transform> KeyValuePair = ObjectPool.instance.Preload(bulletPatternEntry.bulletPrefab.prefabIndex);
					bulletInfo.bullet = KeyValuePair.Key.GetComponent<Bullet>();
					bulletInfo.timeRange = new FloatRange(timeOfGameState, timeOfGameState + timeToNewGameState);
					bulletInfo.position = shooterInfo.position;
					bulletInfo.facing = GetUnitMoveVector(shooterInfo);
					bulletInfo.damageMultiplier = bulletPatternAbility.damageMultiplier;
					bulletInfos.Add(bulletInfo);
				}
			}
			output.bulletInfos = bulletInfos.ToArray();
			return output;
		}

		(float? time, Vector2 newUnitPosition, Vector2 newBulletPosition) GetIntersectionInfo (GameState.UnitInfo unitInfo, GameState.BulletInfo bulletInfo, float durationOfCheck)
		{
			(float? time, Vector2 newUnitPosition, Vector2 newBulletPosition) output = (null, Vector2.zero, Vector2.zero);
			Bullet bullet = bulletInfo.bullet;
			Unit unit = unitInfo.unit;
			float bulletDuration = Mathf.Min(durationOfCheck, bulletInfo.timeRange.max - Time.time);
			LineSegment2D bulletMovementLineSegment = new LineSegment2D(bulletInfo.position, bulletInfo.position + bulletInfo.facing * bullet.moveSpeed * bulletDuration);
			LineSegment2D unitMovementLineSegment = new LineSegment2D(unitInfo.position, GetUnitMovementDestination(unitInfo, durationOfCheck));
			if (Physics2D.GetIgnoreLayerCollision(unit.gameObject.layer, bullet.gameObject.layer))
			{
				output.newBulletPosition = bulletMovementLineSegment.end;
				output.newUnitPosition = unitMovementLineSegment.end;
				return output;
			}
			Stroke2D bulletPathShape = Stroke2D.Straight(bulletMovementLineSegment.start, bulletMovementLineSegment.end, bullet.radius * 2);
			Stroke2D unitPathShape = Stroke2D.Straight(unitMovementLineSegment.start, unitMovementLineSegment.end, unit.radius * 2);
			Shape2D pathOverlapShape = bulletPathShape.IntersectionOfConvexPolygonForConvexPolygon_Fast(unitPathShape);
			if (pathOverlapShape != null)
			{
				Vector2 pathOverlapShapeAverage = VectorExtensions.GetAverage(pathOverlapShape.corners);
				float unitDistanceToPathOverlapShapeAverage = (unitInfo.position - pathOverlapShapeAverage).magnitude - unit.radius;
				if (unitInfo.hp > bullet.damageAmountOnHitDestructable * bulletInfo.damageMultiplier)
				{
					if (unitDistanceToPathOverlapShapeAverage < 0)
						unitDistanceToPathOverlapShapeAverage = 0;
					output.newUnitPosition = unitMovementLineSegment.GetPointWithDirectedDistance(unitDistanceToPathOverlapShapeAverage);
				}
				else
					output.newUnitPosition = unitMovementLineSegment.end;
				if (bulletInfo.hitsBeforeDespawnRemaining == 1)
				{
					float bulletDistanceToPathOverlapShapeAverage = (bulletInfo.position - pathOverlapShapeAverage).magnitude;
					if (bulletDistanceToPathOverlapShapeAverage < 0)
						bulletDistanceToPathOverlapShapeAverage = 0;
					output.newBulletPosition = bulletMovementLineSegment.GetPointWithDirectedDistance(bulletDistanceToPathOverlapShapeAverage);
				}
				else
					output.newBulletPosition = bulletMovementLineSegment.end;
				output.time = unitDistanceToPathOverlapShapeAverage / unit.moveSpeed;
			}
			return output;
		}

		(float? time, Vector2 newUnitPosition, Vector2 newBulletPosition) GetIntersectionInfo (GameState.UnitInfo unitInfo, GameState.BulletInfo bulletInfo)
		{
			return GetIntersectionInfo(unitInfo, bulletInfo, bulletInfo.timeRange.max - Time.time);
		}

		Vector2 GetUnitMovementDestination (GameState.UnitInfo unitInfo, float durationOfCheck)
		{
			return unitInfo.position + GetUnitMoveVector(unitInfo) * durationOfCheck;
		}

		Vector2 GetUnitMoveVector (GameState.UnitInfo unitInfo)
		{
			Player player = unitInfo.unit.player;
			MovementType movementType = player.movementType;
			Vector2 moveVector;
			if (movementType == MovementType.PullUnits)
				moveVector = ((Vector2) player.cursor.trs.position - unitInfo.position).normalized * unitInfo.moveSpeed;
			else if (movementType == MovementType.PushUnits)
				moveVector = (unitInfo.position - (Vector2) player.cursor.trs.position).normalized * unitInfo.moveSpeed;
			else
			{
				moveVector = player.cursor.trs.position - player.cursor.previousPositions[0];
				moveVector = Vector2.ClampMagnitude(moveVector, unitInfo.moveSpeed);
			}
			return moveVector;
		}

		GameState.UnitInfo[] GetUnitsToAffectByAura (GameState gameState, GameState.UnitInfo casterInfo, AuraAbility auraAbility)
		{
			List<GameState.UnitInfo> output = new List<GameState.UnitInfo>();
			for (int i = 0; i < gameState.unitInfos.Length; i ++)
			{
				GameState.UnitInfo unitInfo = gameState.unitInfos[i];
				if (auraAbility.playersIAffect.Contains(unitInfo.unit.player) && IsUnitInRangeOfAura(unitInfo, casterInfo, auraAbility))
					output.Add(unitInfo);
			}
			return output.ToArray();
		}

		bool IsUnitInRangeOfAura (GameState.UnitInfo unitInfo, GameState.UnitInfo casterInfo, AuraAbility auraAbility)
		{
			return (unitInfo.position - casterInfo.position).magnitude < auraAbility.range + unitInfo.unit.radius + casterInfo.unit.radius;
		}

		void Act ()
		{
			HandleMovement ();
			HandleAbilities ();
			HealUnits ();
		}

		void HandleMovement ()
		{
			cursor.MoveTo (currentAction.cursorPosition);
			if (currentAction.movementType == MovementType.PullUnits)
			{
				pullMultiplier = 1;
				PullUnits ();
			}
			else if (currentAction.movementType == MovementType.PushUnits)
			{
				pullMultiplier = -1;
				PullUnits ();
			}
			else
				MoveUnits ();
		}

		public override void OnGainedUnit (Unit unit)
		{
			unit.Init ();
			for (int i = 0; i < unit.abilities.Length; i ++)
			{
				Ability ability = unit.abilities[i];
				int i2 = i;
				ability.shouldUse = () => { return currentAction.useAbilities.Contains(i2); };
			}
		}

		public struct AbilityEvent
		{
			public int abilityIndex;
			public GameState.UnitInfo[] unitInfos;
			public Action<GameState.UnitInfo[]> actionOnUnits;
			public float time;
			public bool mustHappenOnTime;

			public AbilityEvent (int abilityIndex, GameState.UnitInfo[] unitInfos, Action<GameState.UnitInfo[]> actionOnUnits, float time, bool mustHappenOnTime)
			{
				this.abilityIndex = abilityIndex;
				this.unitInfos = unitInfos;
				this.actionOnUnits = actionOnUnits;
				this.time = time;
				this.mustHappenOnTime = mustHappenOnTime;
			}
		}

		public struct Action
		{
			public int indexOfStartTime;
			public MovementType movementType;
			public int[] useAbilities;
			public Vector2 cursorPosition;
			public bool isOffensiveToEnemy;
			public GameState gameStateAfterAction;
			public float value;
		}

		public struct GameState
		{
			public UnitInfo[] unitInfos;
			public BulletInfo[] bulletInfos;
			public AbilityEvent[] eventsToHappen;
			public float myTotalWeightedHp;
			public float enemyTotalWeightedHp;

			public struct UnitInfo
			{
				public Unit unit;
				public FloatRange timeRange;
				public Vector2 position;
				public float hp;
				public float moveSpeed;
				public float[] abilitiesCooldownTimeRemaining;
				public float[] abilitiesChannelTimeRemaining;
			}

			public struct BulletInfo
			{
				public Bullet bullet;
				public FloatRange timeRange;
				public Vector2 position;
				public Vector2 facing;
				public uint hitsBeforeDespawnRemaining;
				public float damageMultiplier;
			}
		}
	}
}