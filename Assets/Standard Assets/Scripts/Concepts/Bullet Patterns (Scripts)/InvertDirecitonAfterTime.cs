﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace RTS
{
	[CreateAssetMenu]
	public class ShootBulletPatternThenInvertBulletDirectionAfterTime : AimWhereFacing
	{
		public float fractionOfLifetimeTillInvert;

		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab)
		{
			Bullet[] output = base.Shoot(spawner, bulletPrefab);
			for (int i = 0; i < output.Length; i ++)
			{
				Bullet bullet = output[i];
				bullet.StartCoroutine(InvertRoutine (bullet));
			}
			return output;
		}

		public virtual IEnumerator InvertRoutine (Bullet bullet)
		{
			yield return new WaitForSeconds(bullet.lifetime * fractionOfLifetimeTillInvert);
			Vector2 newVelocity = -bullet.rigid.velocity;
			bullet.rigid.velocity = newVelocity;
			bullet.trs.up = newVelocity;
		}
	}
}