using UnityEngine;

namespace RTS
{
	public class SetSelfMoveSpeedAbility : Ability
	{
		public Unit unit;
		public float moveSpeed;

		public override void Do ()
		{
			if (!isCoolingDown && !doAnimation)
				SetSelfMoveSpeed ();
			base.Do ();
		}

		public void SetSelfMoveSpeed ()
		{
			unit.moveSpeed = moveSpeed;
		}
	}
}