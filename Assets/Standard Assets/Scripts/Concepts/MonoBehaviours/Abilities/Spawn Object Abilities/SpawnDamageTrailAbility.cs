using UnityEngine;

namespace RTS
{
	public class SpawnDamageTrailAbility : SpawnObjectAbility
	{
		public override void DoSpawnObject ()
		{
			DamageTrail damageTrail = ObjectPool.instance.SpawnComponent<DamageTrail>(spawnablePrefab.prefabIndex, spawnTrs.position);
			damageTrail.gameObject.layer = spawnTrs.gameObject.layer;
			damageTrail.sourceTrs = spawnTrs;
			if (duration > 0)
				ObjectPool.instance.DelayDespawn (damageTrail.prefabIndex, damageTrail.gameObject, damageTrail.trs, duration);
		}
	}
}