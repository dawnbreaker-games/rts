﻿using Extensions;
using UnityEngine;
using System.Collections;

namespace RTS
{
	public class Bomb : Bullet
	{
		public float explodeRange;
		public bool explodeOnHit;
		public bool explodeOnDisable;
		public int[] playerIndicesExplosionHits = new int[0];
		public float explodeDamage;
		public Transform rangeIndicatorTrs;
		bool hasExploded;

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (rangeIndicatorTrs != null)
				rangeIndicatorTrs.SetWorldScale (Vector3.one * explodeRange);
		}
#endif

		public override void OnEnable ()
		{
			base.OnEnable ();
			hasExploded = false;
		}

        public override void OnTriggerEnter2D (Collider2D other)
        {
			base.OnTriggerEnter2D (other);
			if (!hasExploded && explodeOnHit)
				Explode ();
		}

		public void Explode ()
		{
			hasExploded = true;
			for (int i = 0; i < playerIndicesExplosionHits.Length; i ++)
			{
				int playerIndex = playerIndicesExplosionHits[i];
				Player player = Game.players[playerIndex];
				for (int i2 = 0; i2 < player.units.Length; i2 ++)
				{
					Unit unit = player.units[i2];
					if ((unit.trs.position - trs.position).magnitude <= explodeRange + unit.radius)
						unit.TakeDamage (explodeDamage * damageMultiplier);
				}
				for (int i2 = 0; i2 < player.buildings.Length; i2 ++)
				{
					Building building = player.buildings[i2];
					if ((building.trs.position - trs.position).magnitude <= explodeRange + building.radius)
						building.TakeDamage (explodeDamage * damageMultiplier);
				}
			}
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			if (!hasExploded && explodeOnDisable)
				Explode ();
		}
	}
}