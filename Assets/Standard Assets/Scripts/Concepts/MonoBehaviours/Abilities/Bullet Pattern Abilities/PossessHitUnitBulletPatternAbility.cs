using Extensions;
using UnityEngine;
using System.Collections;

namespace RTS
{
	public class PossessHitUnitBulletPatternAbility : BulletPatternAbility
	{
		[HideInInspector]
		public Player player;
		[HideInInspector]
		public Color playerColor;
		[HideInInspector]
		public int playerLayer;
		public float duration;

		void Awake ()
		{
			player = shooter.player;
			playerColor = shooter.spriteRenderer.color;
			playerLayer = shooter.gameObject.layer;
		}

		public override Bullet[] Shoot ()
		{
			Bullet[] output = base.Shoot();
			for (int i = 0; i < output.Length; i ++)
			{
				Bullet bullet = output[i];
				bullet.onHit = (Collider2D collider) =>
					{
						Unit unit = collider.GetComponent<Unit>();
						if (unit != null && !(unit is Building))
							GameManager.instance.StartCoroutine(PossessRoutine (unit));
					};
			}
			return output;
		}

		IEnumerator PossessRoutine (Unit unit)
		{
			Player enemyPlayer = unit.player;
			enemyPlayer.units = enemyPlayer.units.Remove(unit);
			player.GainUnit (unit);
			Color previousColor = unit.spriteRenderer.color;
			int previousLayer = unit.gameObject.layer;
			unit.onDeath += (Unit unit) => { unit.spriteRenderer.color = previousColor; unit.gameObject.layer = previousLayer; };
			unit.spriteRenderer.color = playerColor;
			unit.gameObject.layer = playerLayer;
			yield return new WaitForSeconds(duration);
			if (unit.player == player)
				unit.Death ();
		}
	}
}