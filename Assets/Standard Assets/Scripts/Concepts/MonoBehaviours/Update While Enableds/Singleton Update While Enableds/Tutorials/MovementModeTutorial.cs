using Extensions;

namespace RTS
{
	public class MovementModeTutorial : Tutorial, IUpdatable
	{
		public Player.MovementType movementType;

		void OnEnable ()
		{
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			if (Player.instance.movementType == movementType)
				Finish ();
		}

		void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
		}
	}
}