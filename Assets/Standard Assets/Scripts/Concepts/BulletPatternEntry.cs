using System;
using UnityEngine;

namespace RTS
{
	[Serializable]
	public class BulletPatternEntry
	{
		public string name;
		public BulletPattern bulletPattern;
		public Bullet bulletPrefab;
		public Transform spawner;

		public BulletPatternEntry ()
		{
		}

		public BulletPatternEntry (BulletPatternEntry bulletPatternEntry)
		{
			name = bulletPatternEntry.name;
			bulletPattern = bulletPatternEntry.bulletPattern;
			bulletPrefab = bulletPatternEntry.bulletPrefab;
			spawner = bulletPatternEntry.spawner;
		}
		
		public virtual Bullet[] Shoot ()
		{
			return bulletPattern.Shoot(spawner, bulletPrefab);
		}
	}
}