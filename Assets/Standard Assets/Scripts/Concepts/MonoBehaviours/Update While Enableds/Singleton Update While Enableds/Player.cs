using System;
using Extensions;
using UnityEngine;
using UnityEngine.InputSystem;
using System.Collections.Generic;

namespace RTS
{
	[ExecuteInEditMode]
	public class Player : SingletonUpdateWhileEnabled<Player>
	{
		public Cursor cursor;
		public Unit[] units = new Unit[0];
		public Building[] buildings = new Building[0];
		[HideInInspector]
		public MovementType movementType;
		public byte raceIndex;
		public byte[] enemyPlayerIndices = new byte[0];
		// [HideInInspector]
		public Player[] enemyPlayers = new Player[0];
		[HideInInspector]
		public int pullMultiplier = 1;
		float maxJoystickCursorDistanceFromOrigin;
		bool leftClickInput;
		bool previousLeftClickInput;
		bool rightClickInput;
		bool previousRightClickInput;

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				// units = GetComponentsInChildren<Unit>();
				// for (int i = 0; i < units.Length; i ++)
				// {
				// 	Unit unit = units[i];
				// 	unit.OnEnable ();
				// 	if (unit is Building)
				// 	{
				// 		units = units.RemoveAt(i);
				// 		i --;
				// 	}
				// }
				// buildings = GetComponentsInChildren<Building>();
				// Game.Instance.Awake ();
				// enemyPlayers = new Player[0];
				// for (int i = 0; i < enemyPlayerIndices.Length; i ++)
				// {
				// 	int enemyPlayerIndex = enemyPlayerIndices[i];
				// 	enemyPlayers = enemyPlayers.Add(Game.players[enemyPlayerIndex]);
				// }
				return;
			}
#endif
			if (Game.localPlayer == this)
				enemyPlayers = new Player[] { Game.enemyPlayer };
			else
				enemyPlayers = new Player[] { Game.localPlayer };
			base.OnEnable ();
			maxJoystickCursorDistanceFromOrigin = CameraScript.Instance.viewSize.magnitude / 2;
			cursor.Init ();
			for (int i = 0; i < units.Length; i ++)
			{
				Unit unit = units[i];
				OnGainedUnit (unit);
			}
			for (int i = 0; i < buildings.Length; i ++)
			{
				Building building = buildings[i];
				building.Init ();
			}
		}

		public override void DoUpdate ()
		{
			leftClickInput = InputManager.LeftClickInput;
			rightClickInput = InputManager.RightClickInput;
			if (InputManager.instance.inputDevice == InputManager.InputDevice.KeyboardAndMouse)
				cursor.MoveTo (CameraScript.instance.camera.ScreenToWorldPoint((Vector2) InputManager.MousePosition));
			else
				cursor.MoveTo ((Vector2) InputManager.LeftStickInput * maxJoystickCursorDistanceFromOrigin * ((Vector2) InputManager.RightStickInput).magnitude);
			HandleAbilities ();
			if (leftClickInput && !previousLeftClickInput)
			{
				pullMultiplier *= -1;
				if (pullMultiplier > 0)
					movementType = MovementType.PullUnits;
				else
					movementType = MovementType.PushUnits;
			}
			if (rightClickInput)
			{
				movementType = MovementType.MoveUnits;
				MoveUnits ();
			}
			else
				PullUnits ();
			HealUnits ();
			previousLeftClickInput = leftClickInput;
			previousRightClickInput = rightClickInput;
		}

		public virtual void PullUnits ()
		{
			for (int i = 0; i < units.Length; i ++)
			{
				Unit unit = units[i];
				Vector2 move = (cursor.trs.position - unit.trs.position) * pullMultiplier;
				unit.Move (move.normalized * unit.moveSpeed);
				unit.SetFacing (move);
			}
		}

		public virtual void MoveUnits ()
		{
			Vector2 move = cursor.trs.position - cursor.previousPositions[0];
			for (int i = 0; i < units.Length; i ++)
			{
				Unit unit = units[i];
				unit.Move (move);
				unit.SetFacing (move);
			}
		}

		public virtual void HandleAbilities ()
		{
			for (int i = 0; i < units.Length; i ++)
			{
				Unit unit = units[i];
				for (int i2 = 0; i2 < unit.abilities.Length; i2 ++)
				{
					Ability ability = unit.abilities[i2];
					if (ability.shouldUse())
						unit.DoAbility (i2);
				}
			}
		}

		public void HealUnits ()
		{
			for (int i = 0; i < units.Length; i ++)
			{
				Unit unit = units[i];
				unit.Heal (unit.hpRegenRate * Time.deltaTime);
			}
		}

		public void GainUnit (Unit unit)
		{
			unit.player = this;
			units = units.Add(unit);
			OnGainedUnit (unit);
		}

		public virtual void OnGainedUnit (Unit unit)
		{
			unit.Init ();
			if (unit.abilities.Length == 0)
				return;
			if (InputManager.instance.inputDevice == InputManager.InputDevice.KeyboardAndMouse)
			{
				unit.abilities[0].shouldUse = () => { return Keyboard.current.digit1Key.isPressed; };
				unit.abilities[1].shouldUse = () => { return Keyboard.current.digit2Key.isPressed; };
				unit.abilities[2].shouldUse = () => { return Keyboard.current.digit3Key.isPressed; };
			}
			else
			{
				unit.abilities[0].shouldUse = () => { return Gamepad.current.leftShoulder.isPressed; };
				unit.abilities[1].shouldUse = () => { return Gamepad.current.rightStickButton.isPressed; };
				unit.abilities[2].shouldUse = () => { return Gamepad.current.rightShoulder.isPressed; };
			}
		}

		[Serializable]
		public class Cursor
		{
			public Transform trs;
			public LineRenderer movementIndicatorLineRenderer;
			public float minMovemnetIndicatorPointSeparation;
			public float targetMovementIndicatorLength;
			[HideInInspector]
			public List<Vector3> previousPositions = new List<Vector3>();
			float movementIndicatorLength;
			Vector2 previousPosition;
			List<MovementIndicatorPoint> movementIndicatorPoints = new List<MovementIndicatorPoint>();

			public void Init ()
			{
				Player player = trs.GetComponentInParent<Player>();
				if (!(player is VirtualPlayer2))
				{
					if (InputManager.Instance.inputDevice == InputManager.InputDevice.KeyboardAndMouse)
						trs.position = CameraScript.instance.camera.ScreenToWorldPoint((Vector2) InputManager.MousePosition);
					else
						trs.position = (Vector2) InputManager.LeftStickInput * instance.maxJoystickCursorDistanceFromOrigin * ((Vector2) InputManager.RightStickInput).magnitude;
				}
				previousPosition = trs.position;
			}

			public void MoveTo (Vector2 position)
			{
				trs.position = position;
				if ((Vector2) trs.position != previousPosition)
				{
					Vector2 move = (Vector2) trs.position - previousPosition;
					float distanceToPreviousPosition = move.magnitude;
					float totalMoveAmount = distanceToPreviousPosition;
					while (totalMoveAmount > 0)
					{
						float moveAmount = Mathf.Min(minMovemnetIndicatorPointSeparation, totalMoveAmount);
						previousPosition += move.normalized * moveAmount;
						previousPositions.Add(previousPosition);
						movementIndicatorPoints.Add(new MovementIndicatorPoint(previousPosition, moveAmount));
						movementIndicatorLength += moveAmount;
						totalMoveAmount -= moveAmount;
					}
					if (movementIndicatorPoints.Count > 0)
					{
						totalMoveAmount = movementIndicatorLength - targetMovementIndicatorLength;
						while (totalMoveAmount > 0)
						{
							MovementIndicatorPoint movementIndicatorPoint = movementIndicatorPoints[0];
							movementIndicatorPoints.RemoveAt(0);
							previousPositions.RemoveAt(0);
							movementIndicatorLength -= movementIndicatorPoint.distanceToPreviousPoint;
							if (movementIndicatorPoints.Count == 0)
								break;
							totalMoveAmount -= movementIndicatorPoint.distanceToPreviousPoint;
						}
						previousPosition = previousPositions[0];
						move = (previousPosition - (Vector2) previousPositions[1]).normalized;
						totalMoveAmount = targetMovementIndicatorLength - movementIndicatorLength;
						while (totalMoveAmount > 0)
						{
							float moveAmount = Mathf.Min(minMovemnetIndicatorPointSeparation, totalMoveAmount);
							previousPosition += move.normalized * moveAmount;
							previousPositions.Insert(0, previousPosition);
							movementIndicatorPoints.Insert(0, new MovementIndicatorPoint(previousPosition, moveAmount));
							movementIndicatorLength += moveAmount;
							totalMoveAmount -= moveAmount;
						}
					}
					previousPosition = trs.position;
					movementIndicatorLineRenderer.positionCount = previousPositions.Count;
					movementIndicatorLineRenderer.SetPositions(previousPositions.ToArray());
				}
			}
		}

		public struct MovementIndicatorPoint
		{
			public Vector2 position;
			public float distanceToPreviousPoint;

			public MovementIndicatorPoint (Vector2 position, float distanceToPreviousPoint)
			{
				this.position = position;
				this.distanceToPreviousPoint = distanceToPreviousPoint;
			}
		}

		public enum MovementType
		{
			PullUnits,
			PushUnits,
			MoveUnits
		}
	}
}