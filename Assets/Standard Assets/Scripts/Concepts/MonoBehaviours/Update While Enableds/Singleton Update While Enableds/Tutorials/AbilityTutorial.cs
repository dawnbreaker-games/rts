using Extensions;
using UnityEngine;

namespace RTS
{
	public class AbilityTutorial : Tutorial, IUpdatable
	{
		public byte abilityIndex;
		public float channelTime;
		float channelTimeRemaining;

		void OnEnable ()
		{
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			Ability ability = Player.instance.units[0].abilities[abilityIndex];
			if (ability.shouldUse())
			{
				channelTimeRemaining -= Time.deltaTime;
				if (channelTimeRemaining <= 0)
					Finish ();
			}
			else
				channelTimeRemaining = channelTime;
		}

		void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
		}
	}
}