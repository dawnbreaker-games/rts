using UnityEngine;

namespace RTS
{
	public class MultiplayerGame : Game
	{
		public new static MultiplayerGame instance;
		public GameObject waitForOtherPlayerIndicatorGo;

		public override void Awake ()
		{
			instance = this;
		}

		public void Init ()
		{
			base.Awake ();
		}
		
		public override void GameOver ()
		{
			_SceneManager.instance.LoadSceneWithoutTransition ("Init");
		}
	}
}