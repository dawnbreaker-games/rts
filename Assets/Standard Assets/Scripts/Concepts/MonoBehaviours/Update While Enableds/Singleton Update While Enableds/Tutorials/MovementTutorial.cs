using Extensions;

namespace RTS
{
	public class MovementTutorial : Tutorial, IUpdatable
	{
		public Player.MovementType movementType;
		bool hasUsedMovementType;

		void OnEnable ()
		{
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			if (Player.instance.movementType != movementType)
			{
				if (hasUsedMovementType)
				{
					Player.instance.movementType = movementType;
					if (movementType == Player.MovementType.PullUnits)
						Player.instance.pullMultiplier = 1;
					else if (movementType == Player.MovementType.PushUnits)
						Player.instance.pullMultiplier = -1;
				}
			}
			else
				hasUsedMovementType = true;
		}

		void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
		}
	}
}