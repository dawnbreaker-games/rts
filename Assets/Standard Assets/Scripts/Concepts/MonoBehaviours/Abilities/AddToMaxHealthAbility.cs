using Extensions;
using UnityEngine;
using System.Collections;

namespace RTS
{
	public class AddToMaxHealthAbility : Ability
	{
        public Unit unit;
        public float amount;

		public override void Do ()
		{
			if (!isCoolingDown && !doAnimation)
                AddToMaxHealth ();
			base.Do ();
		}

		void AddToMaxHealth ()
		{
            unit.maxHp += amount;
		}
	}
}