using TMPro;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace RTS
{
	public class MainMenu : SingletonMonoBehaviour<MainMenu>
	{
		public CanvasGroup canvasGroup;
		public GameObject playTutorialFirstTipGo;
		public GameObject playSingleplayerWhileWaitingForMultiplayerTipGo;
		public Transform menusParent;
		public bool[] useRacesForLocalRandom;
		public bool[] useRacesForEnemyRandom;
		public TMP_Text[] localPlayerRaceTexts = new TMP_Text[0];
		public TMP_Text enemyPlayerRaceText;
		public Transform buttonsParent;
		public Transform buttonsParentPositionTrsWhenExtraButtonsRevealed;
		public float extraButtonsRevealRate;
		Vector2 buttonsParentPositionWhenExtraButtonsUnrevealed;

		public override void Awake ()
		{
			base.Awake ();
			Game.localPlayerRaceIndex = 0;
			Game.enemyPlayerRaceIndex = 0;
			buttonsParentPositionWhenExtraButtonsUnrevealed = buttonsParent.position;
		}

		public void SetShowPlayTuturialFirstTip (bool show)
		{
			SaveAndLoadManager.saveData.showPlayTutorialFirstTip = show;
		}

		public void OnSingleplayerButtonClicked (int menuTrsSiblingIndexToActivateIfNoTip)
		{
			if (SaveAndLoadManager.saveData.showPlayTutorialFirstTip)
				playTutorialFirstTipGo.SetActive(true);
			else
			{
				gameObject.SetActive(false);
				menusParent.GetChild(menuTrsSiblingIndexToActivateIfNoTip).gameObject.SetActive(true);
				buttonsParent.position = buttonsParentPositionWhenExtraButtonsUnrevealed;
			}
		}

		public void SetShowWaitForMultiplayerTip (bool show)
		{
			SaveAndLoadManager.saveData.showWaitForMultiplayerTip = show;
		}

		public void OnMultiplayerButtonClicked ()
		{
			if (SaveAndLoadManager.saveData.showPlaySingleplayerWhileWaitingForMultiplayerTip)
				playSingleplayerWhileWaitingForMultiplayerTipGo.SetActive(true);
			else
			{
				canvasGroup.interactable = false;
				NetworkManager.instance.Connect ();
			}
		}

		public void SetShowPlaySingleplayerWhileWaitingForMultiplayerTip (bool show)
		{
			SaveAndLoadManager.saveData.showPlaySingleplayerWhileWaitingForMultiplayerTip = show;
		}

		public void SetLocalPlayerRace (int raceIndex)
		{
			Game.localPlayerRaceIndex = (byte) raceIndex;
			for (int i = 0; i < localPlayerRaceTexts.Length; i ++)
			{
				TMP_Text localPlayerRaceText = localPlayerRaceTexts[i];
				localPlayerRaceText.text = "Race " + (Game.localPlayerRaceIndex + 1);
			}
		}

		public void SetEnemyRace (int raceIndex)
		{
			Game.enemyPlayerRaceIndex = (byte) raceIndex;
			enemyPlayerRaceText.text = "Race " + (Game.enemyPlayerRaceIndex + 1);
		}

		public void SetUseRandomLocalRace (bool useRandomRace)
		{
			if (useRandomRace)
			{
				SetLocalRandomRace ();
				for (int i = 0; i < localPlayerRaceTexts.Length; i ++)
				{
					TMP_Text localPlayerRaceText = localPlayerRaceTexts[i];
					localPlayerRaceText.text = "Random";
				}
			}
			else
			{
				for (int i = 0; i < localPlayerRaceTexts.Length; i ++)
				{
					TMP_Text localPlayerRaceText = localPlayerRaceTexts[i];
					localPlayerRaceText.text = "Race " + (Game.localPlayerRaceIndex + 1);
				}
			}
		}

		public void ToggleUseRaceForLocalRandom (int raceIndex)
		{
			useRacesForLocalRandom[raceIndex] = !useRacesForLocalRandom[raceIndex];
			SetLocalRandomRace ();
		}

		void SetLocalRandomRace ()
		{
			List<byte> racesToUseForLocalRandom = new List<byte>();
			for (int i = 0; i < useRacesForLocalRandom.Length; i ++)
			{
				bool useRaceForLocalRandom = useRacesForLocalRandom[i];
				if (useRaceForLocalRandom)
					racesToUseForLocalRandom.Add((byte) i);
			}
			if (racesToUseForLocalRandom.Count == 0)
			{
				for (byte i = 0; i < useRacesForLocalRandom.Length; i ++)
					racesToUseForLocalRandom.Add(i);
			}
			Game.localPlayerRaceIndex = racesToUseForLocalRandom[Random.Range(0, racesToUseForLocalRandom.Count)];
		}

		public void SetUseRandomEnemyRace (bool useRandomRace)
		{
			if (useRandomRace)
			{
				SetEnemyRandomRace ();
				enemyPlayerRaceText.text = "Random";
			}
			else
				enemyPlayerRaceText.text = "Race " + (Game.enemyPlayerRaceIndex + 1);
		}

		public void ToggleUseRaceForEnemyRandom (int raceIndex)
		{
			useRacesForEnemyRandom[raceIndex] = !useRacesForEnemyRandom[raceIndex];
			SetEnemyRandomRace ();
		}

		void SetEnemyRandomRace ()
		{
			List<byte> racesToUseForEnemyRandom = new List<byte>();
			for (int i = 0; i < useRacesForEnemyRandom.Length; i ++)
			{
				bool useRaceForEnemyRandom = useRacesForEnemyRandom[i];
				if (useRaceForEnemyRandom)
					racesToUseForEnemyRandom.Add((byte) i);
			}
			if (racesToUseForEnemyRandom.Count == 0)
			{
				for (byte i = 0; i < useRacesForEnemyRandom.Length; i ++)
					racesToUseForEnemyRandom.Add(i);
			}
			Game.enemyPlayerRaceIndex = racesToUseForEnemyRandom[Random.Range(0, racesToUseForEnemyRandom.Count)];
		}

		public void OnSkirmishButtonClicked ()
		{
			if (SaveAndLoadManager.saveData.showPlayTutorialFirstTip)
				playTutorialFirstTipGo.SetActive(true);
			else
				StartCoroutine(RevealExtraButtonsRoutine ());
		}

		public void RevealExtraButtons ()
		{
			StartCoroutine(RevealExtraButtonsRoutine ());
		}

		IEnumerator RevealExtraButtonsRoutine ()
		{
			float revealAmount = 0;
			while (revealAmount < 1)
			{
				revealAmount += extraButtonsRevealRate * Time.unscaledDeltaTime;
				buttonsParent.position = Vector2.Lerp(buttonsParentPositionWhenExtraButtonsUnrevealed, buttonsParentPositionTrsWhenExtraButtonsRevealed.position, revealAmount);
				yield return new WaitForEndOfFrame();
			}
		}
	}
}