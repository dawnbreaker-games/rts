using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace RTS
{
	[ExecuteInEditMode]
	public class Game : SingletonMonoBehaviour<Game>
	{
		public Player[] potentialLocalPlayers = new Player[0];
		public Player[] potentialEnemyPlayers = new Player[0];
        public static Player[] players = new Player[0];
		public static byte localPlayerRaceIndex;
		public static byte enemyPlayerRaceIndex;
		public GameObject wonIndicatorGo;
		public GameObject lostIndicatorGo;
		public static Player localPlayer;
		public static Player enemyPlayer;

		public override void Awake ()
		{
#if UNITY_EDITOR
			if (Application.isPlaying)
			{
#endif
			base.Awake ();
			Time.timeScale = 1;
#if UNITY_EDITOR
			}
			else
				return;
#endif
			localPlayer = potentialLocalPlayers[localPlayerRaceIndex];
			enemyPlayer = potentialEnemyPlayers[enemyPlayerRaceIndex];
			players = new Player[2] { localPlayer, enemyPlayer };
			Bullet.playersBulletsDict.Clear();
			for (byte i = 0; i < players.Length; i ++)
				Bullet.playersBulletsDict.Add(i, new List<Bullet>());
			localPlayer.gameObject.SetActive(true);
			enemyPlayer.gameObject.SetActive(true);
		}

		public virtual void GameOver ()
		{
			if (localPlayer.units.Length == 0 || localPlayer.buildings.Length == 0)
				lostIndicatorGo.SetActive(true);
			else
				wonIndicatorGo.SetActive(true);
		}

		void OnDestroy ()
		{
			localPlayerRaceIndex = 0;
			enemyPlayerRaceIndex = 0;
		}
	}
}