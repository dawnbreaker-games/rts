using UnityEngine;

namespace RTS
{
	public class HealSelfAbility : Ability
	{
		public Unit unit;
		public float amount;

		public override void Do ()
		{
			if (!isCoolingDown && !doAnimation)
				HealSelf ();
			base.Do ();
		}

		public void HealSelf ()
		{
			unit.Heal (amount);
		}
	}
}