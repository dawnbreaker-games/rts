using Extensions;
using UnityEngine;
using System.Collections;

namespace RTS
{
	public class TeleportAbility : Ability
	{
        public Transform teleportTrs;
		public Transform teleportPositionIndicatorTrs;
		public GameObject teleportPositionIndicatorGo;
        public float teleportPositionIndicatorDistanceChangeRate;

		public override void Do ()
		{
			if (!isCoolingDown && !doAnimation)
                Teleport ();
			base.Do ();
		}

		void Teleport ()
		{
			teleportTrs.position = teleportPositionIndicatorTrs.position;
			teleportPositionIndicatorGo.SetActive(false);
		}

		public override IEnumerator ChannelRoutine ()
		{
			teleportPositionIndicatorTrs.localPosition = Vector3.zero;
			teleportPositionIndicatorGo.SetActive(true);
			cooldownIndicatorImage.fillAmount = 0;
			channelTimeRemaining = maxChannelTime;
			while (channelTimeRemaining > 0 && shouldUse())
			{
				channelTimeRemaining -= Time.deltaTime;
				teleportPositionIndicatorTrs.localPosition += Vector3.up * teleportPositionIndicatorDistanceChangeRate * Time.deltaTime;
				yield return new WaitForEndOfFrame();
			}
			Teleport ();
			channelTimeRemaining = 0;
			StartCoroutine(CooldownRoutine ());
		}

		void OnDisable ()
		{
			teleportPositionIndicatorGo.SetActive(false);
		}
	}
}