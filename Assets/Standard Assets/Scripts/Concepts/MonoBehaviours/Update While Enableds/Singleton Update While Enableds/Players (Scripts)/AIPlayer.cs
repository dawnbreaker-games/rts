using System;
using Extensions;
using UnityEngine;
using Unity.MLAgents;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;
using UnityEngine.InputSystem.Controls;
using Random = UnityEngine.Random;

namespace RTS
{
	public class AIPlayer : Player
	{
		public AIAgent aiAgent;
		public Action currentAction;
		public static new AIPlayer instance;
		public static new AIPlayer Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<AIPlayer>();
				return instance;
			}
		}
		int movementTypeCount;

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				base.OnEnable ();
				return;
			}
#endif
			movementTypeCount = Enum.GetNames(typeof(MovementType)).Length;
			for (int i = 0; i < units.Length; i ++)
			{
				Unit unit = units[i];
				OnGainedUnit (unit);
			}
			if (Instance != this && handleMultipleInstances != MultipleInstancesHandlingType.KeepAll)
			{
				if (handleMultipleInstances == MultipleInstancesHandlingType.DestroyNew)
				{
					Destroy(gameObject);
					return;
				}
				else
					Destroy(instance.gameObject);
			}
			if (persistant)
				DontDestroyOnLoad(gameObject);
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public override void DoUpdate ()
		{
			Think ();
			Act ();
		}

		void Think ()
		{
			aiAgent.RequestDecision();
			aiAgent.EndEpisode();
			Academy.Instance.EnvironmentStep();
		}

		void Act ()
		{
			HandleMovement ();
			HandleAbilities ();
			HealUnits ();
		}

		void HandleMovement ()
		{
			cursor.MoveTo (currentAction.cursorPosition);
			if (currentAction.movementType == MovementType.PullUnits)
			{
				pullMultiplier = 1;
				PullUnits ();
			}
			else if (currentAction.movementType == MovementType.PushUnits)
			{
				pullMultiplier = -1;
				PullUnits ();
			}
			else
				MoveUnits ();
		}

		public override void OnGainedUnit (Unit unit)
		{
			unit.Init ();
			for (int i = 0; i < unit.abilities.Length; i ++)
			{
				Ability ability = unit.abilities[i];
				int i2 = i;
				ability.shouldUse = () => { return currentAction.useAbilities != null && currentAction.useAbilities.Length > i2 && currentAction.useAbilities[i2]; };
			}
		}

		public struct Action
		{
			public MovementType movementType;
			public bool[] useAbilities;
			public Vector2 cursorPosition;
		}
	}
}