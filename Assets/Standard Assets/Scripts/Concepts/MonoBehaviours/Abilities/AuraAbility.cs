using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace RTS
{
	public class AuraAbility : Ability
	{
		public Unit source;
		public float range;
		public float duration;
		public int[] playerIndicesIAffect = new int[0];
		[HideInInspector]
		public Player[] playersIAffect = new Player[0];
        public bool canAffectSelf;
        public bool canAffectSameUnitType;
		public GameObject indicatorGo;

#if UNITY_EDITOR
		void OnValidate ()
		{
			Transform indicatorTrs = indicatorGo.GetComponent<Transform>();
			indicatorTrs.SetWorldScale (Vector3.one * (range + source.radius) * 2);
		}
#endif

		public override void OnEnable ()
		{
			base.OnEnable ();
			playersIAffect = new Player[playerIndicesIAffect.Length];
			for (int i = 0; i < playerIndicesIAffect.Length; i ++)
			{
				int playerIndexIAffect = playerIndicesIAffect[i];
				playersIAffect[i] = Game.players[playerIndexIAffect];
			}
		}

		protected Unit[] AffectUnits (Action<Unit> effect)
		{
			List<Unit> units = new List<Unit>();
			for (int i = 0; i < playersIAffect.Length; i ++)
			{
				Player player = playersIAffect[i];
				for (int i2 = 0; i2 < player.units.Length; i2 ++)
				{
					Unit unit = player.units[i2];
					if (!(unit is Building) && (unit.type != source.type || (canAffectSelf && unit == source) || (canAffectSameUnitType && unit.type == source.type)) && IsUnitInRange(unit))
					{
						effect (unit);
						units.Add(unit);
					}
				}
			}
			return units.ToArray();
		}

		protected bool IsUnitInRange (Unit unit)
		{
			return (unit.trs.position - source.trs.position).magnitude < range + unit.radius + source.radius;
		}
	}
}