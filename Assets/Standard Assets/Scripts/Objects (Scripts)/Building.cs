using Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace RTS
{
	[ExecuteInEditMode]
	public class Building : Unit, IUpdatable
	{
		public int spawnCount;
		public float spawnDuration;
		public Unit unitPrefab;
		public Transform spawnTrs;
		public Image spawnTimerIndicator;
		float spawnTimer;

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				// Game.Instance.Awake ();
				// if (Game.players.Length > playerIndex)
				// 	player = Game.players[playerIndex];
				return;
			}
#endif
			base.OnEnable ();
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public override void Death ()
		{
			if (dead)
				return;
			Destroy(gameObject);
			player.buildings = player.buildings.Remove(this);
			OnDeath ();
		}

		public void DoUpdate ()
		{
			spawnTimer += Time.deltaTime;
			if (spawnTimer >= spawnDuration)
			{
				spawnTimer -= spawnDuration;
				for (int i = 0; i < spawnCount; i ++)
				{
					Unit unit = ObjectPool.instance.SpawnComponent<Unit>(unitPrefab.prefabIndex, spawnTrs.position);
					player.GainUnit (unit);
				}
			}
			spawnTimerIndicator.fillAmount = spawnTimer / spawnDuration;
		}

		public override void SetFacing (Vector2 facing)
		{
		}

		void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
		}
	}
}